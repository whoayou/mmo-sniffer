package com.ss.sniffer.document;

import java.io.File;

import com.ss.sniffer.manager.ElementBuilderManager;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.ui.model.packet.protocol.DefaultPacketDescription;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.protocol.DefaultProtocol;
import com.ss.sniffer.ui.model.packet.protocol.PacketDescription;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;
import com.ss.sniffer.ui.model.packet.protocol.builder.ElementBuilder;
import com.ss.sniffer.ui.model.packet.protocol.builder.RootElementBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import rlib.data.AbstractFileDocument;
import rlib.util.ClassUtils;
import rlib.util.VarTable;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * XML парсер протоколов из xml.
 * 
 * @author Ronn
 */
public class ProtocolDocument extends AbstractFileDocument<Array<Protocol>> {

	private static final String BUILDER_NAME_KEY = "name";
	private static final String BUILDER_TYPE_KEY = "type";

	private static final String DESCRIPTION_OPCODE_KEY = "opcode";
	private static final String DESCRIPTION_NAME_KEY = BUILDER_NAME_KEY;

	private static final String PROTOCOL_VERSION_KEY = "version";
	private static final String PROTOCOL_NAME_KEY = DESCRIPTION_NAME_KEY;
	private static final String PROTOCOL_MODEL_KEY = "model";

	private static final String XML_NODE_PROTOCOL = "protocol";
	private static final String XML_NODE_LIST = "list";
	private static final String XML_NODE_SERVER = "server";
	private static final String XML_NODE_CLIENT = "client";
	private static final String XML_NODE_PACKET_ELEMENT = "element";
	private static final String XML_NODE_PACKET = "packet";

	public ProtocolDocument(final File file) {
		super(file);
	}

	@Override
	protected Array<Protocol> create() {
		return ArrayFactory.newArray(Protocol.class);
	}

	@Override
	protected void parse(final Document doc) {

		final VarTable vars = VarTable.newInstance();

		for(Node node = doc.getFirstChild(); node != null; node = node.getNextSibling()) {

			if(node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!XML_NODE_LIST.equals(node.getNodeName())) {
				continue;
			}

			for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

				if(child.getNodeType() != Node.ELEMENT_NODE || !XML_NODE_PROTOCOL.equals(child.getNodeName())) {
					continue;
				}

				vars.parse(child);

				final String name = vars.getString(PROTOCOL_NAME_KEY);
				final String version = vars.getString(PROTOCOL_VERSION_KEY);

				final Class<PacketModel> modelClass = ClassUtils.getClass(vars.getString(PROTOCOL_MODEL_KEY));

				if(modelClass == null) {
					continue;
				}

				final DefaultProtocol protocol = new DefaultProtocol(modelClass, name, version);

				final Array<PacketDescription> descriptions = parseDescription(child);

				for(final PacketDescription description : descriptions) {
					protocol.addDescription(description);
				}

				result.add(protocol);
			}
		}
	}

	/**
	 * Рекурсивный парс билдеров элементов пакета.
	 * 
	 * @param root рутовый билдер для билдеров в этом узле.
	 * @param node узел для парса билдеров.
	 */
	private void parseBuilder(final ElementBuilder root, final Node node) {

		final ElementBuilderManager manager = ElementBuilderManager.getInstance();

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!XML_NODE_PACKET_ELEMENT.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final String type = vars.getString(BUILDER_TYPE_KEY);
			final String name = vars.getString(BUILDER_NAME_KEY);

			final ElementBuilder builder = manager.getBuilder(type);

			if(builder == null) {
				continue;
			}

			builder.setElementName(name);

			parseBuilder(builder, child);

			root.addChildren(builder);
		}
	}

	/**
	 * Парс описания структуры пакетов.
	 * 
	 * @param node узел с набором описаний пакетов.
	 * @return список описаний пакетов.
	 */
	private Array<PacketDescription> parseDescription(final Node node) {

		final Array<PacketDescription> result = ArrayFactory.newArray(PacketDescription.class);

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(XML_NODE_CLIENT.equals(child.getNodeName())) {
				result.addAll(parseDescription(child, false));
			} else if(XML_NODE_SERVER.equals(child.getNodeName())) {
				result.addAll(parseDescription(child, true));
			}
		}

		return result;
	}

	/**
	 * Парс описаний класса пакетов.
	 * 
	 * @param node узел с пакетами одного класса.
	 * @param server серверные ли это пакеты.
	 * @return список описаний пакетов.
	 */
	private Array<PacketDescription> parseDescription(final Node node, final boolean server) {

		final Array<PacketDescription> descriptions = ArrayFactory.newArray(PacketDescription.class);

		final VarTable vars = VarTable.newInstance();

		for(Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {

			if(child.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			if(!XML_NODE_PACKET.equals(child.getNodeName())) {
				continue;
			}

			vars.parse(child);

			final PacketType type = server ? PacketType.SERVER_PACKET : PacketType.CLIENT_PACKET;

			final String name = vars.getString(DESCRIPTION_NAME_KEY);
			final int opcode = Integer.decode(vars.getString(DESCRIPTION_OPCODE_KEY));

			final ElementBuilder root = new RootElementBuilder();
			parseBuilder(root, child);

			descriptions.add(new DefaultPacketDescription(root, type, name, opcode));
		}

		return descriptions;
	}
}
