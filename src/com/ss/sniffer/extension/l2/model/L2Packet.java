package com.ss.sniffer.extension.l2.model;

import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.model.packet.AbstractUIPacket;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author Ronn
 */
public class L2Packet extends AbstractUIPacket {

	public L2Packet(final Packet packet) {
		super(packet);
	}

	@Override
	protected void prepare() {

		final Packet packet = getPacket();

		final byte[] data = packet.getData();

		final ByteBuffer buffer = ByteBuffer.allocate(data.length).order(ByteOrder.LITTLE_ENDIAN);
		buffer.clear();
		buffer.put(data);
		buffer.flip();

		final int size = buffer.getShort() & 0xFFFF;
		final int opcode = buffer.get() & 0xFF;

		setSize(size);
		setOpcode(opcode);

		buffer.clear();
		buffer.put(data, 3, data.length - 3);

		setBuffer(buffer);
	}
}
