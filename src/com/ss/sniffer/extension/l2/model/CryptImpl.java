package com.ss.sniffer.extension.l2.model;

import com.ss.sniffer.net.Crypt;

import java.util.Arrays;

/**
 * @author Ronn
 */
public class CryptImpl implements Crypt {

	/** криптор */
	private final L2Crypt gameCrypt;

	/** готов ли криптор */
	private boolean ready;

	public CryptImpl() {
		this.gameCrypt = new L2Crypt();
	}

	@Override
	public void decrypt(final byte[] data, final int offset, final int length, final boolean server) {

		if(!isReady() && server && data[2] == 0x2E) {
			gameCrypt.setKey(Arrays.copyOfRange(data, 2, 18));
			ready = true;
			return;
		}

		if(isReady()) {
			gameCrypt.decrypt(data, offset, length);
		}
	}

	@Override
	public boolean isReady() {
		return ready;
	}
}
