package com.ss.sniffer.extension.spaceshift.model.packet.builder;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.builder.AbstractElementBuilder;
import com.ss.sniffer.ui.model.packet.protocol.element.StringElement;
import rlib.util.array.Array;

import java.nio.ByteBuffer;

/**
 * Реализация билдера короткой строки для SS протокола.
 * 
 * @author Ronn
 */
public class SpaceShiftShortString extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		byte[] array = new byte[1];

		buffer.get(array);

		final String hexLength = NetUtil.toHEX(array);

		final int length = array[0] & 0xFF;

		array = new byte[length * 2];

		buffer.get(array);
		buffer.position(buffer.position() - length * 2);

		final StringBuilder builder = new StringBuilder(length);

		for(int i = 0; i < length; i++) {
			builder.append(buffer.getChar());
		}

		container.add(new StringElement(getElementName(), hexLength + " - " + NetUtil.toHEX(array), builder.toString()));
	}

	@Override
	public String getName() {
		return "spaceShiftShortString";
	}
}
