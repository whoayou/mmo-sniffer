package com.ss.sniffer.extension.tera.model;

import com.ss.sniffer.extension.tera.model.crypt.TeraCrypt;
import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.Crypt;

/**
 * Описание типа криптора для Tera Online.
 * 
 * @author Ronn
 */
public class TeraCryptType implements CryptType {

	@Override
	public Crypt create() {
		return new TeraCrypt();
	}

	@Override
	public String getName() {
		return "Tera-Online Crypt";
	}

	@Override
	public boolean validate(final PacketModel model) {
		return model instanceof TeraPacketModel;
	}
}
