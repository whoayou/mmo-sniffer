package com.ss.sniffer.extension.tera.model;

import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.PacketBuilder;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.UIPacket;
import rlib.util.array.Array;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author Ronn
 */
public class TeraPacketModel implements PacketModel {

	@Override
	public UIPacket build(final Packet packet) {
		return new TeraPacket(packet);
	}

	@Override
	public String getName() {
		return "Tera-Online Model";
	}

	@Override
	public int getOpcodeLength() {
		return 2;
	}

	@Override
	public void postCrypt(final Packet source, final Array<Packet> container, final PacketBuilder builder) {

		final byte[] data = source.getData();

		if(data.length < 2) {
			container.add(source);
			return;
		}

		final ByteBuffer buffer = ByteBuffer.allocate(data.length).order(ByteOrder.LITTLE_ENDIAN);
		buffer.clear();
		buffer.put(data);
		buffer.flip();

		int size = buffer.getShort() & 0xFFFF;

		if(size < 2 || size > Short.MAX_VALUE || size >= buffer.remaining() + 2) {
			container.add(source);
			return;
		}

		byte[] array = new byte[size];

		buffer.position(buffer.position() - 2);
		buffer.get(array);

		container.add(builder.build(array, source.getSource(), source.getDestination(), source.getSessionId(), source.getTimestamp(), source.getType() == PacketType.SERVER_PACKET));

		while(buffer.remaining() > 2) {

			size = buffer.getShort() & 0xFFFF;

			if(size < 2 || size > Short.MAX_VALUE || size >= buffer.remaining() + 2) {
				break;
			}

			array = new byte[size];

			buffer.position(buffer.position() - 2);
			buffer.get(array);

			container.add(builder.build(array, source.getSource(), source.getDestination(), source.getSessionId(), source.getTimestamp(), source.getType() == PacketType.SERVER_PACKET));
		}
	}

	@Override
	public void preCrypt(final Packet source, final Array<Packet> container, final PacketBuilder builder) {
		container.add(source);
	}

	@Override
	public boolean validate(final CryptType cryptType) {
		return cryptType instanceof TeraCryptType;
	}

	@Override
	public boolean validate(final Packet packet) {
		return true;
	}
}
