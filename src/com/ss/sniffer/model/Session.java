package com.ss.sniffer.model;

import com.ss.sniffer.ui.model.listeners.SessionListener;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;

import rlib.util.array.Array;

/**
 * Интерфейс для реализации сессии снифера.
 * 
 * @author Ronn
 */
public interface Session {

	/**
	 * Добавляет во все реализованные слушатели.
	 */
	public void addListener(SessionListener object);

	/**
	 * Добавление в сессию UI пакета.
	 * 
	 * @param uiPacket новый UI пакет.
	 */
	public void addPacket(UIPacket uiPacket);

	/**
	 * Удаление всех полученных пакетов в сессии.
	 */
	public void clear();

	/**
	 * @return ид сессии.
	 */
	public int getId();

	/**
	 * Получение всех хранящихся в сессии пакетов.
	 * 
	 * @param container контейнер пакетов.
	 */
	public void getPackets(Array<Packet> container);

	/**
	 * @return новый протокол.
	 */
	public Protocol getProtocol();

	/**
	 * Перезагрузка пакетов.
	 */
	public void reload();

	/**
	 * Удаяет из реализованых слушателей.
	 */
	public void removeListener(SessionListener object);

	/**
	 * Установка нового протокола пакетов.
	 * 
	 * @param protocol новый протокол.
	 */
	public void setProcotol(Protocol protocol);
}
