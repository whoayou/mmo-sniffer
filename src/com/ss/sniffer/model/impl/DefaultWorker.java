package com.ss.sniffer.model.impl;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.model.listeners.MessageListener;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.ui.state.UIState;
import com.ss.sniffer.ui.state.impl.MainUIState;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Дефолтная реализация работника снифера.
 * 
 * @author Ronn
 */
public class DefaultWorker implements Worker {

	/** список слушателей сообщений */
	private final Array<MessageListener> messageListeners;

	/** все активные сессии */
	private final Array<Session> sessions;

	/** пакетная модель */
	private final PacketModel packetModel;

	/** УИ */
	private final MainUIState mainStage;

	/** текущая выбраьнная сессия */
	private Session currentSession;

	public DefaultWorker(final MainUIState mainStage) {
		this.packetModel = Config.PACKET_MODEL;
		this.messageListeners = ArrayFactory.newArray(MessageListener.class);
		this.sessions = ArrayFactory.newConcurrentArraySet(Session.class);
		this.mainStage = mainStage;
	}

	@Override
	public void addListener(final Object object) {
		if(object instanceof MessageListener) {
			addMessageListener((MessageListener) object);
		}
	}

	@Override
	public void addMessageListener(final MessageListener listener) {
		messageListeners.add(listener);
	}

	@Override
	public Session getCurrentSession() {
		return currentSession;
	}

	/**
	 * Получить сессию к указанному пакету.
	 * 
	 * @param packet присланный пакет.
	 * @return сессия для этого пакета.
	 */
	protected Session getSession(final Packet packet) {

		final Array<Session> sessions = getSessions();

		final int sessionId = packet.getSessionId();

		sessions.readLock();
		try {

			for(final Session session : sessions.array()) {

				if(session == null) {
					break;
				}

				if(session.getId() == sessionId) {
					return session;
				}
			}

		} finally {
			sessions.readUnlock();
		}

		sessions.writeLock();
		try {

			for(final Session session : sessions.array()) {

				if(session == null) {
					break;
				}

				if(session.getId() == sessionId) {
					return session;
				}
			}

			final Session session = mainStage.addSession(sessionId);
			sessions.add(session);
			return session;
		} finally {
			sessions.writeUnlock();
		}
	}

	/**
	 * @return список текущих сессий.
	 */
	protected Array<Session> getSessions() {
		return sessions;
	}

	@Override
	public UIState getUIStage() {
		return mainStage;
	}

	@Override
	public void removeListener(final Object object) {
		if(object instanceof MessageListener) {
			removeMessageListener((MessageListener) object);
		}
	}

	@Override
	public void removeMessageListener(final MessageListener listener) {
		messageListeners.fastRemove(listener);
	}

	@Override
	public void removeSession(final Session session) {
		session.clear();
		sessions.fastRemove(session);
	}

	@Override
	public void sendMessage(final String message) {

		for(final MessageListener listener : messageListeners.array()) {
			if(listener == null) {
				break;
			}

			listener.addMessage(message);
		}
	}

	@Override
	public void sendPacket(final Packet packet) {

		final UIPacket uiPacket = packetModel.build(packet);

		if(uiPacket != null) {
			final Session session = getSession(packet);
			session.addPacket(uiPacket);
		}
	}

	@Override
	public void setCurrentSession(final Session session) {
		this.currentSession = session;
	}
}
