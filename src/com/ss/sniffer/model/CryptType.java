package com.ss.sniffer.model;

import com.ss.sniffer.net.Crypt;

/**
 * Интерфейс, описывающий тип используемого криптора.
 * 
 * @author Ronn
 */
public interface CryptType {

	/**
	 * Создание нового экземпляра криптора.
	 * 
	 * @return новый криптор.
	 */
	public Crypt create();

	/**
	 * @return название криптора.
	 */
	public String getName();

	/**
	 * Проверка совместимости криптора с пакетной моделью.
	 * 
	 * @param model проверяемая модель.
	 * @return совместимы ли.
	 */
	public boolean validate(PacketModel model);
}
