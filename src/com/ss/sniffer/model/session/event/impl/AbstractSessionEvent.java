package com.ss.sniffer.model.session.event.impl;

import com.ss.sniffer.model.session.event.SessionEvent;

/**
 * Реализация базового события сессии.
 * 
 * @author Ronn
 */
public abstract class AbstractSessionEvent implements SessionEvent {

	private final Object source;
	private final Object target;

	public AbstractSessionEvent(final Object source, final Object target) {
		this.source = source;
		this.target = target;
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public Object getTarget() {
		return target;
	}
}
