package com.ss.sniffer.model.session.event.impl;

import com.ss.sniffer.model.session.event.SessionEvent;
import com.ss.sniffer.model.session.event.SessionEventType;

/**
 * Пакет уведомляющий о перезагрузке пакетов.
 * 
 * @author Ronn
 */
public class ReloadPacketsEvent implements SessionEvent {

	public static final SessionEventType<ReloadPacketsEvent> RELOAD_PACKETS = new SessionEventType<ReloadPacketsEvent>() {

		@Override
		public String toString() {
			return "RELOAD_PACKETS";
		}
	};

	@Override
	public Object getSource() {
		return null;
	}

	@Override
	public Object getTarget() {
		return null;
	}

	@Override
	public SessionEventType<?> getType() {
		return RELOAD_PACKETS;
	}
}
