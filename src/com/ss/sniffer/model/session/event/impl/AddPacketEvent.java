package com.ss.sniffer.model.session.event.impl;

import com.ss.sniffer.model.session.event.SessionEventType;

/**
 * Реализация события по добавению пакета в сессию.
 * 
 * @author Ronn
 */
public class AddPacketEvent extends AbstractSessionEvent {

	public static final SessionEventType<AddPacketEvent> ADD_PACKET = new SessionEventType<AddPacketEvent>() {

		@Override
		public String toString() {
			return "ADD_PACKET";
		}
	};

	public AddPacketEvent(final Object source, final Object target) {
		super(source, target);
	}

	@Override
	public SessionEventType<?> getType() {
		return ADD_PACKET;
	}
}
