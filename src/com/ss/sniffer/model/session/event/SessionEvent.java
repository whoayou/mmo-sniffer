package com.ss.sniffer.model.session.event;

/**
 * Интерфейс для реализации события сессии.
 * 
 * @author Ronn
 */
public interface SessionEvent {

	/**
	 * @return источник события.
	 */
	public Object getSource();

	/**
	 * @return цель события.
	 */
	public Object getTarget();

	/**
	 * @return тип события.
	 */
	public SessionEventType<?> getType();
}
