package com.ss.sniffer.model.session.event;

/**
 * Интерфейс для реализации типа события сессии.
 * 
 * @author Rronn
 */
public interface SessionEventType<T extends SessionEvent> {

}
