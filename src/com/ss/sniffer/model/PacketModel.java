package com.ss.sniffer.model;

import com.ss.sniffer.net.PacketBuilder;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.net.packet.Packet;

import rlib.util.array.Array;

/**
 * Интерфейс, описывающий пакетную модель.
 * 
 * @author Ronn
 */
public interface PacketModel {

	/**
	 * Построение UI пакета поверх сетевого пакета.
	 * 
	 * @param packet сетевой пакет.
	 * @return UI пакет.
	 */
	public UIPacket build(Packet packet);

	/**
	 * @return название модели.
	 */
	public String getName();

	/**
	 * @return кол-во байтов в опкоде.
	 */
	public int getOpcodeLength();

	/**
	 * Перестроить пакет, если это не обходимо после использования криптора.
	 * 
	 * @param source исходный пакет.
	 * @param container контейнер для результата.
	 * @param builder строитель спетевых пакетов.
	 */
	public void postCrypt(Packet source, Array<Packet> container, PacketBuilder builder);

	/**
	 * Перестроить пакет, если это не обходимо перед использованием криптора.
	 * 
	 * @param source исходный пакет.
	 * @param container контейнер для результата.
	 * @param builder строитель спетевых пакетов.
	 */
	public void preCrypt(Packet source, Array<Packet> container, PacketBuilder builder);

	/**
	 * Проверка совместимости криптора с пакетной моделью.
	 * 
	 * @param cryptType проверяемый криптор.
	 * @return совместимы ли.
	 */
	public boolean validate(CryptType cryptType);

	/**
	 * Проверка корректности пакета.
	 * 
	 * @param packet проверяемый пакет.
	 * @return можно ли продолжать работать с пакетом.
	 */
	public boolean validate(Packet packet);
}
