package com.ss.sniffer.net;

/**
 * Интерфейс для реализации криптора для снифера.
 * 
 * @author Ronn
 */
public interface Crypt {

	/**
	 * Декриптовать пакет.
	 * 
	 * @param data данные пакета.
	 * @param offset отступ в пакете.
	 * @param length длинна пакета.
	 * @param server серверный пакет ли.
	 */
	public void decrypt(byte[] data, int offset, int length, boolean server);

	/**
	 * @return подготовлен ли криптор.
	 */
	public boolean isReady();
}
