package com.ss.sniffer.net;

import com.ss.sniffer.net.packet.Packet;

/**
 * Интерфейс для реализации билдера сетевых пакетов.
 * 
 * @author Ronn
 */
public interface PacketBuilder {

	/**
	 * Построить сетевой пакет.
	 * 
	 * @param data массив данных пакета.
	 * @param sourceIp адресс источника.
	 * @param destinationIp адресс получателя.
	 * @param sessionId ид сессии.
	 * @param timestamp время получения.
	 * @param server серверный ли пакет.
	 * @return новый сетевой пакет.
	 */
	public Packet build(byte[] data, int sourceIp, int destinationIp, int sessionId, long timestamp, boolean server);
}
