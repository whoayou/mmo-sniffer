package com.ss.sniffer.net.impl;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.Crypt;
import com.ss.sniffer.net.Sniffer;
import javafx.application.Platform;
import javafx.concurrent.Task;

import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.PacketType;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Обработчик спойманных пакетов.
 * 
 * @author Ronn
 */
public abstract class SnifferPacketHandler extends Task<Void> implements PcapPacketHandler<Worker> {

	private static final Logger LOGGER = LoggerManager.getLogger(PcapPacketHandler.class);

	/** читальщик пакетов */
	private final PacketReader reader;
	/** ядро снифера */
	private final Sniffer sniffer;

	/** контейнер пакетов перде криптом */
	private final Array<Packet> preCrypt;
	/** контейнер пакетов после крипта */
	private final Array<Packet> postCrypt;

	/** текущая пакетная модель */
	private final PacketModel packetModel;
	/** криптор пакетов */
	private final Crypt crypt;

	public SnifferPacketHandler(final Sniffer sniffer) {
		this.crypt = Config.CRYPT_TYPE.create();
		this.packetModel = Config.PACKET_MODEL;
		this.preCrypt = ArrayFactory.newArray(Packet.class);
		this.postCrypt = ArrayFactory.newArray(Packet.class);
		this.reader = new PacketReader();
		this.sniffer = sniffer;
	}

	/**
	 * @return криптор снифера.
	 */
	private Crypt getCrypt() {
		return crypt;
	}

	/**
	 * @return текущая пакетная модель.
	 */
	private PacketModel getPacketModel() {
		return packetModel;
	}

	/**
	 * @return контейнер пакетов после криптора.
	 */
	private Array<Packet> getPostCrypt() {
		return postCrypt;
	}

	/**
	 * @return контейнер пакетов перед криптором.
	 */
	private Array<Packet> getPreCrypt() {
		return preCrypt;
	}

	/**
	 * @return читальщик пакетов.
	 */
	private PacketReader getReader() {
		return reader;
	}

	@Override
	public void nextPacket(final PcapPacket source, final Worker worker) {

		final Array<Packet> preCrypt = getPreCrypt();
		final Array<Packet> postCrypt = getPostCrypt();

		final PacketReader reader = getReader();
		final Crypt crypt = getCrypt();

		try {

			if(!reader.check(source)) {
				return;
			}

			final Packet packet = reader.read(sniffer, source);
			final PacketModel packetModel = getPacketModel();

			if(!packetModel.validate(packet)) {
				return;
			}

			packetModel.preCrypt(packet, preCrypt, reader);

			for(final Packet next : preCrypt) {

				final byte[] data = next.getData();
				final boolean ready = crypt.isReady();

				crypt.decrypt(data, 0, data.length, packet.getType() == PacketType.SERVER_PACKET);

				if(!ready) {
					continue;
				}

				packetModel.postCrypt(next, postCrypt, reader);
			}

			if(postCrypt.isEmpty()) {
				return;
			}

			Runnable updateTask = null;

			if(postCrypt.size() == 1) {
				updateTask = () -> worker.sendPacket(postCrypt.first());
			} else {

				final Array<Packet> container = ArrayFactory.newArray(Packet.class, postCrypt.size());
				container.addAll(postCrypt);

				updateTask = () -> {
					for(final Packet update : container) {
						worker.sendPacket(update);
					}
				};
			}

			if(updateTask != null) {
				Platform.runLater(updateTask);
			}

		} catch(final Exception e) {
			LOGGER.warning(e);
		} finally {
			preCrypt.clear();
			postCrypt.clear();
		}
	}
}
