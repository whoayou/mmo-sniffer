package com.ss.sniffer.net.packet.impl;

import com.ss.sniffer.net.packet.Packet;

import rlib.util.Util;

/**
 * Базовая реализация выснифанного пакета.
 * 
 * @author Ronn
 */
public abstract class AbstractPacket implements Packet {

	/** адресс источника */
	private final int source;
	/** адресс цели */
	private final int destination;

	/** время получения пакета */
	private long timestamp;

	/** ид сессии */
	private int sessionId;

	/** данные пакета */
	private byte[] data;

	public AbstractPacket(final int source, final int destination) {
		this.source = source;
		this.destination = destination;
	}

	@Override
	public byte[] getData() {
		return data;
	}

	@Override
	public int getDestination() {
		return destination;
	}

	@Override
	public int getSessionId() {
		return sessionId;
	}

	@Override
	public int getSource() {
		return source;
	}

	@Override
	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public void setData(final byte[] data) {
		this.data = data;
	}

	@Override
	public void setSessionId(final int sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public void setTimestamp(final long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " source = " + source + ", destination = " + destination + ", sessionId = " + sessionId + ", timestamp = " + Util.formatTime(timestamp) + "\n"
				+ Util.hexdump(data, data.length);
	}
}
