package com.ss.sniffer;

import com.ss.sniffer.ui.UIApplication;

import rlib.util.Util;

/**
 * Запускающий класс снифера.
 * 
 * @author Ronn
 */
public class Start extends UIApplication {

	public static void main(final String[] args) {
		Util.safeExecute(() -> {
			launch(args);
		});
	}

	@Override
	protected void preapreRootClass() {
		Config.ROOT_CLASS = Start.class;
	}
}
