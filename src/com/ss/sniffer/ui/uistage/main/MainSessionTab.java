package com.ss.sniffer.ui.uistage.main;

import com.ss.sniffer.Start;
import com.ss.sniffer.manager.ClassManager;
import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.model.SessionTab;
import com.ss.sniffer.ui.model.listeners.SessionListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import com.ss.sniffer.ui.state.UIState;

import rlib.util.array.Array;

/**
 * Рутовая вкладка сессии, в которой уже находятся SessionTab.
 * 
 * @author Ronn
 */
public class MainSessionTab extends Tab {

	private static final String IMAGE_PATH = "/resource/session_16.png";
	private static final String TITLE = "Сессия:";

	private static Image sessionTabImage;

	private static Image getSessionTabImage() {

		if(sessionTabImage == null) {
			sessionTabImage = new Image(Start.class.getResourceAsStream(IMAGE_PATH));
		}

		return sessionTabImage;
	}

	/** панель с сессионными табами */
	private final TabPane tabPane;

	/** работник снифера */
	private final Worker worker;

	/** пакетная сессия этого таба */
	private Session session;

	public MainSessionTab(final Worker worker) {

		this.worker = worker;

		final Image image = getSessionTabImage();

		if(image != null) {
			setGraphic(new ImageView(image));
		}

		final VBox layout = new VBox();

		setContent(layout);

		tabPane = new TabPane();

		layout.getChildren().add(tabPane);

		tabPane.prefWidthProperty().bind(layout.widthProperty());
		tabPane.prefHeightProperty().bind(layout.heightProperty());

		ObservableList<Tab> tabs = tabPane.getTabs();
		tabs.addListener(new ListChangeListener<Tab>() {

			@Override
			public void onChanged(Change<? extends Tab> change) {

				while(change.next()) {

					if(!change.wasRemoved())
						return;

					for(Tab tab : change.getRemoved()) {
						if(tab instanceof SessionTab)
							((SessionTab) tab).finish();
					}
				}
			}
		});
	}

	public void close() {
		worker.removeSession(session);
	}

	/**
	 * @return пакетная сессия этого таба.
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * Подготовка панели с табами сессии.
	 * 
	 * @param uiStage UI стадия.
	 * @param session рабочая пакетная сессия.
	 */
	public void prepare(final UIState uiStage, final Session session) {

		setText(TITLE + session.getId());

		final ClassManager manager = ClassManager.getInstance();

		final ObservableList<Tab> tabs = tabPane.getTabs();

		final Array<SessionTab> sessionTabs = manager.getAllSessionTabs(uiStage);

		for(final SessionTab sessionTab : sessionTabs) {

			sessionTab.prepare(uiStage, session);

			tabs.add(sessionTab.getTab());

			if(sessionTab instanceof SessionListener) {
				session.addListener((SessionListener) sessionTab);
			}
		}

		setSession(session);
	}

	private void setSession(final Session session) {
		this.session = session;
	}
}
