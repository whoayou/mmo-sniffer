package com.ss.sniffer.ui.uistage.main;

import com.ss.sniffer.manager.ClassManager;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.MenuCategory;
import com.ss.sniffer.ui.state.UIState;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import rlib.util.array.Array;

/**
 * Реализация построения меню с доступными действиями.
 * 
 * @author Ronn
 */
public class MenuBarPanel extends MenuBar {

	public MenuBarPanel(final UIState uiStage, final Worker worker) {

		super();

		final ClassManager manager = ClassManager.getInstance();

		final Array<MenuAction> actions = manager.getAllMenuActions(uiStage);

		final ObservableList<Menu> menus = getMenus();

		for(final MenuCategory category : MenuCategory.values()) {
			// TODO добавить картинки
			final Menu menu = new Menu(category.getName());

			final ObservableList<MenuItem> items = menu.getItems();

			for(final MenuAction action : actions) {

				if(action.getCategory() != category) {
					continue;
				}

				final Image image = action.getImage();

				MenuItem menuItem = null;

				final MenuItem[] children = action.getChildren();

				if(children == null || children.length < 1) {
					menuItem = image == null ? new MenuItem(action.getName()) : new MenuItem(action.getName(), new ImageView(image));
				} else {

					final Menu subMenu = image == null ? new Menu(action.getName()) : new Menu(action.getName(), new ImageView(image));

					for(final MenuItem item : children) {

						item.addEventHandler(ActionEvent.ACTION, event -> action.execute(item, worker));
					}

					subMenu.getItems().addAll(children);

					menuItem = subMenu;
				}

				menuItem.addEventHandler(ActionEvent.ACTION, event -> action.execute(worker));

				items.add(menuItem);
			}

			menus.add(menu);
		}
	}
}
