package com.ss.sniffer.ui.uistage.main;

import com.ss.sniffer.ui.model.WorkerTab;
import com.ss.sniffer.manager.ClassManager;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.state.UIState;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import rlib.util.array.Array;

/**
 * Главная панель с табами.
 * 
 * @author Ronn
 */
public class MainTabPanel extends TabPane {

	public MainTabPanel(final UIState uiStage, final Worker worker) {
		super();

		setSide(Side.TOP);
		setTabClosingPolicy(TabClosingPolicy.ALL_TABS);

		final ObservableList<Tab> tabs = getTabs();

		final ClassManager manager = ClassManager.getInstance();

		final Array<WorkerTab> mainTabs = manager.getAllMainTabs(uiStage);

		for(final WorkerTab mainTab : mainTabs) {

			mainTab.prepare(uiStage, worker);

			worker.addListener(mainTab);

			tabs.add(mainTab.getTab());
		}

		tabs.addListener(new ListChangeListener<Tab>() {

			@Override
			public void onChanged(Change<? extends Tab> change) {

				while(change.next()) {

					if(!change.wasRemoved())
						return;

					for(Tab tab : change.getRemoved()) {

						if(tab instanceof WorkerTab) {
							((WorkerTab) tab).finish();
						} else if(tab instanceof MainSessionTab) {
							((MainSessionTab) tab).close();
						}
					}
				}
			}
		});
	}
}
