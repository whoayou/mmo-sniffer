package com.ss.sniffer.ui.uistage.load;

import com.ss.sniffer.manager.ElementBuilderManager;
import com.ss.sniffer.manager.PacketFieldManager;
import com.ss.sniffer.manager.ClassManager;
import com.ss.sniffer.manager.ExecutorManager;
import com.ss.sniffer.manager.ProtocolManager;
import com.ss.sniffer.ui.state.impl.LoadUIState;

import javafx.application.Platform;
import javafx.concurrent.Task;
import rlib.manager.InitializeManager;

/**
 * Задача по инициализации ресурсов.
 * 
 * @author Ronn
 */
public class LoadTask extends Task<Void> {

	/** стадия загрузки */
	private final LoadUIState loadStage;

	public LoadTask(final LoadUIState loadStage) {
		this.loadStage = loadStage;
	}

	@Override
	protected Void call() throws Exception {

		try {
			InitializeManager.register(ClassManager.class);
			InitializeManager.register(ExecutorManager.class);
			InitializeManager.register(PacketFieldManager.class);
			InitializeManager.register(ElementBuilderManager.class);
			InitializeManager.register(ProtocolManager.class);
			InitializeManager.initialize();
		} catch(final Throwable e) {
			e.printStackTrace();
		}

		Platform.runLater(() -> {
			try {
				loadStage.finish();
			} catch(final Throwable e) {
				e.printStackTrace();
			}
		});

		return null;
	}
}
