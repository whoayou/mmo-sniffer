package com.ss.sniffer.ui.uistage.config;

import org.jnetpcap.PcapIf;
import com.ss.sniffer.net.Sniffer;

/**
 * UI итем для отображения информации об устройстве.
 * 
 * @author Ronn
 */
public class DeviceItem {

	private static final String NULL_DEVICE_NAME = "Не слушать ничего.";

	/** сетевое устройство */
	private final PcapIf device;

	public DeviceItem(final PcapIf device) {
		this.device = device;
	}

	/**
	 * @return сетевое устройство.
	 */
	public PcapIf getDevice() {
		return device;
	}

	@Override
	public String toString() {

		if(device == Sniffer.NULL_DEVICE) {
			return NULL_DEVICE_NAME;
		}

		return device.getName() + " : " + device.getDescription();
	}
}
