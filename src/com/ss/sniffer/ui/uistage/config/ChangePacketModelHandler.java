package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.Config;
import com.ss.sniffer.ui.state.impl.ConfigUIState;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;

/**
 * Хендлер для внесения в конфиг выбранную пакетную модель.
 * 
 * @author Ronn
 */
public class ChangePacketModelHandler implements EventHandler<ActionEvent> {

	private final ConfigUIState configStage;

	public ChangePacketModelHandler(final ConfigUIState configStage) {
		this.configStage = configStage;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void handle(final ActionEvent event) {

		final Object source = event.getSource();

		if(source instanceof ComboBox) {

			final ComboBox<PacketModelItem> choiceBox = (ComboBox<PacketModelItem>) source;

			final SingleSelectionModel<PacketModelItem> selection = choiceBox.getSelectionModel();

			if(selection.isEmpty()) {
				Config.PACKET_MODEL = null;
				return;
			}

			final PacketModelItem item = selection.getSelectedItem();

			Config.PACKET_MODEL = item.getPacketModel();
		}

		configStage.update();
	}
}
