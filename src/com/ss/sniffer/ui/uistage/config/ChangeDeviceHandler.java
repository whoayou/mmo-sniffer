package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.Config;
import com.ss.sniffer.ui.state.impl.ConfigUIState;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;

/**
 * Хендлер для внесения в конфиг выбранное сетевое устройство.
 * 
 * @author Ronn
 */
public class ChangeDeviceHandler implements EventHandler<ActionEvent> {

	private final ConfigUIState configStage;

	public ChangeDeviceHandler(final ConfigUIState configStage) {
		this.configStage = configStage;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void handle(final ActionEvent event) {

		final Object source = event.getSource();

		if(source instanceof ComboBox) {

			final ComboBox<DeviceItem> choiceBox = (ComboBox<DeviceItem>) source;

			final SingleSelectionModel<DeviceItem> selection = choiceBox.getSelectionModel();

			if(selection.isEmpty()) {
				Config.NETWORK_DEVICE = null;
				return;
			}

			final DeviceItem item = selection.getSelectedItem();

			Config.NETWORK_DEVICE = item.getDevice();
		}

		configStage.update();
	}
}
