package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.model.CryptType;

/**
 * UI итем для отображения информации об крпторе.
 * 
 * @author Ronn
 */
public class CryptTypeItem {

	/** тип криптора */
	private final CryptType crypt;

	public CryptTypeItem(final CryptType crypt) {
		this.crypt = crypt;
	}

	/**
	 * @return криптор.
	 */
	public CryptType getCrypt() {
		return crypt;
	}

	@Override
	public String toString() {
		return crypt.getName();
	}
}
