package com.ss.sniffer.ui.model.session.tabs;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.session.event.SessionEvent;
import com.ss.sniffer.model.session.event.impl.AddPacketEvent;
import com.ss.sniffer.model.session.event.impl.ReloadPacketsEvent;
import com.ss.sniffer.ui.Constans;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.control.packet.table.PacketTableView;
import com.ss.sniffer.ui.control.packet.viewer.MultiPacketViewer;
import com.ss.sniffer.ui.model.SessionTab;
import com.ss.sniffer.ui.model.listeners.SessionListener;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.ui.state.UIState;

/**
 * Реализация просмоторщика пакетов.
 * 
 * @author Ronn
 */
public class PacketView extends Tab implements SessionTab, SessionListener {

	private static final String PACKET_VIEWER_IMAGE_NAME = "packet_viewer_16";
	private static final String CLEAR_IMAGE_NAME = "clear_16";
	private static final String PLAY_IMAGE_NAME = "play_16";
	private static final String PAUSE_IMAGE_NAME = "pause_16";
	private static final String FILTER_BOX_NAME = "Только уникальные";
	private static final String TITLE = "Просмотр пакетов";

	private static ImageView pauseImageView;
	private static ImageView playImageView;
	private static ImageView clearImageView;
	private static ImageView tableImageView;

	public static ImageView getClearImageView() {

		if(clearImageView == null) {
			clearImageView = new ImageView(UIUtils.loadImage(CLEAR_IMAGE_NAME));
		}

		return clearImageView;
	}

	public static ImageView getPauseImageView() {

		if(pauseImageView == null) {
			pauseImageView = new ImageView(UIUtils.loadImage(PAUSE_IMAGE_NAME));
		}

		return pauseImageView;
	}

	public static ImageView getPlayImageView() {

		if(playImageView == null) {
			playImageView = new ImageView(UIUtils.loadImage(PLAY_IMAGE_NAME));
		}

		return playImageView;
	}

	public static ImageView getTableImageView() {

		if(tableImageView == null) {
			tableImageView = new ImageView(UIUtils.loadImage(PACKET_VIEWER_IMAGE_NAME));
		}

		return tableImageView;
	}

	private final VBox tableLayout;

	/** таблицы пакетов */
	private final PacketTableView allPacketTable;
	private final PacketTableView uniquePacketTable;

	/** переключатель таблиц */
	private final CheckBox uniqueSwicher;

	/** кнопка очистки */
	private final Button clear;
	/** кнопка паузы */
	private final Button pause;

	/** отображатель самих пакетов */
	private final MultiPacketViewer packetViewer;

	/** текущая пакетная сессия */
	private Session session;

	public PacketView() {
		setText(TITLE);
		setGraphic(getTableImageView());

		final HBox root = new HBox(2);
		root.setAlignment(Pos.CENTER);

		setContent(root);

		packetViewer = new MultiPacketViewer(root);

		tableLayout = new VBox();
		tableLayout.setAlignment(Pos.CENTER);

		allPacketTable = new PacketTableView(false, packetViewer);
		allPacketTable.prefWidthProperty().bind(root.widthProperty());
		allPacketTable.prefHeightProperty().bind(root.heightProperty());

		uniquePacketTable = new PacketTableView(true, packetViewer);
		uniquePacketTable.setVisible(false);
		uniquePacketTable.prefWidthProperty().bind(root.widthProperty());
		uniquePacketTable.prefHeightProperty().bind(root.heightProperty());

		uniqueSwicher = new CheckBox(FILTER_BOX_NAME);
		uniqueSwicher.setAlignment(Pos.CENTER_RIGHT);
		uniqueSwicher.addEventHandler(ActionEvent.ACTION, new TableChangeHandler(this));

		clear = new Button();
		clear.setGraphic(getClearImageView());
		clear.addEventHandler(ActionEvent.ACTION, event -> clear());

		pause = new Button();
		pause.setGraphic(getPauseImageView());
		pause.addEventHandler(ActionEvent.ACTION, event -> {
			if(pause.getGraphic() == getPauseImageView()) {
				pause.setGraphic(getPlayImageView());
			} else if(pause.getGraphic() == getPlayImageView()) {
				pause.setGraphic(getPauseImageView());
			}
		});

		final HBox hBox = new HBox(3);
		hBox.setAlignment(Pos.CENTER_LEFT);
		hBox.getChildren().addAll(pause, clear, uniqueSwicher);
		hBox.prefWidthProperty().bind(tableLayout.widthProperty());

		HBox.setMargin(clear, Constans.INSETS_4);
		HBox.setMargin(pause, Constans.INSETS_4);
		HBox.setMargin(uniqueSwicher, Constans.INSETS_3);

		final ObservableList<Node> children = tableLayout.getChildren();
		children.add(hBox);
		children.add(allPacketTable);

		VBox.setMargin(uniqueSwicher, Constans.INSETS_2);

		root.getChildren().addAll(tableLayout, packetViewer);

		tableLayout.prefWidthProperty().bind(root.widthProperty().divide(2));
		packetViewer.prefWidthProperty().bind(root.widthProperty().divide(2));
	}

	/**
	 * Очистка таблиц.
	 */
	private void clear() {

		final PacketTableView allPacketTable = getAllPacketTable();
		allPacketTable.getItems().clear();

		final PacketTableView uniquePacketTable = getUniquePacketTable();
		uniquePacketTable.getItems().clear();

		final Session session = getSession();
		session.clear();
	}

	@Override
	public void finish() {

		final PacketTableView allPacketTable = getAllPacketTable();
		allPacketTable.getItems().clear();

		final PacketTableView uniquePacketTable = getUniquePacketTable();
		uniquePacketTable.getItems().clear();
	}

	/**
	 * @return таблица со всеми пакетами.
	 */
	private PacketTableView getAllPacketTable() {
		return allPacketTable;
	}

	/**
	 * @return текущая пакетная сессия.
	 */
	private Session getSession() {
		return session;
	}

	@Override
	public Tab getTab() {
		return this;
	}

	/**
	 * @return таблица с уникальными пакетами.
	 */
	private PacketTableView getUniquePacketTable() {
		return uniquePacketTable;
	}

	/**
	 * @return переключатель таблиц.
	 */
	private CheckBox getUniqueSwicher() {
		return uniqueSwicher;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void notify(final Session session, final SessionEvent event) {

		if(event.getType() == AddPacketEvent.ADD_PACKET) {

			if(pause.getGraphic() == getPlayImageView()) {
				return;
			}

			final UIPacket packet = (UIPacket) event.getTarget();

			final PacketTableView allPacketTable = getAllPacketTable();
			final PacketTableView uniquePacketTable = getUniquePacketTable();

			if(allPacketTable.isVisible()) {
				allPacketTable.addPacket(packet);
			}

			if(uniquePacketTable.isVisible()) {
				uniquePacketTable.addPacket(packet);
			}

		} else if(event.getType() == ReloadPacketsEvent.RELOAD_PACKETS) {

			final PacketTableView allPacketTable = getAllPacketTable();
			allPacketTable.getItems().clear();

			final PacketTableView uniquePacketTable = getUniquePacketTable();
			uniquePacketTable.getItems().clear();
		}
	}

	@Override
	public void prepare(final UIState uiStage, final Session session) {
		setSession(session);
	}

	/**
	 * @param session текущая пакетная сессия.
	 */
	private void setSession(final Session session) {
		this.session = session;
		this.packetViewer.setSession(session);
	}

	/**
	 * Переключение режима таблицы.
	 */
	public void switchTable() {

		final ObservableList<Node> children = tableLayout.getChildren();

		final CheckBox uniqueSwicher = getUniqueSwicher();

		final PacketTableView allPacketTable = getAllPacketTable();
		final PacketTableView uniquePacketTable = getUniquePacketTable();

		final boolean selected = uniqueSwicher.isSelected();

		if(selected && allPacketTable.isVisible()) {

			allPacketTable.setVisible(false);
			uniquePacketTable.setVisible(true);

			children.remove(allPacketTable);
			children.add(uniquePacketTable);
		} else if(!selected && uniquePacketTable.isVisible()) {

			allPacketTable.setVisible(true);
			uniquePacketTable.setVisible(false);

			children.remove(uniquePacketTable);
			children.add(allPacketTable);
		}
	}
}
