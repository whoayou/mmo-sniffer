package com.ss.sniffer.ui.model.menu.actions;

import java.io.File;
import java.io.IOException;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.MenuCategory;
import com.ss.sniffer.ui.state.UIState;
import com.ss.sniffer.util.Utils;

import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Реализация акшена для сохранения пакетов сессии.
 * 
 * @author Ronn
 */
public class SaveAsAction implements MenuAction {

	private static final String SAVE_AS_IMAGE_NAME = "save_as_16";
	private static final String TITLE = "Сохранение сессии";
	private static final String ACTION_NAME = "Сохранить сессию как...";

	/** окно выбора файла */
	private final FileChooser fileChooser;

	/** стадия УИ для выбора файла */
	private final Stage stage;

	public SaveAsAction() throws IOException {

		fileChooser = new FileChooser();
		fileChooser.setTitle(TITLE);
		fileChooser.setInitialDirectory(Utils.getRootFolderFromClass(Config.ROOT_CLASS).getCanonicalFile());

		final ObservableList<ExtensionFilter> filters = fileChooser.getExtensionFilters();
		filters.add(new ExtensionFilter(NetUtil.SESSION_DUMP_FORMAT, "*" + NetUtil.SESSION_DUMP_FORMAT));

		stage = new Stage();
		stage.initModality(Modality.WINDOW_MODAL);
	}

	@Override
	public void execute(final MenuItem item, final Worker worker) {
	}

	@Override
	public synchronized void execute(final Worker worker) {

		final UIState uiStage = worker.getUIStage();
		final Session session = worker.getCurrentSession();

		if(session == null) {
			UIUtils.showMessage(uiStage, "Отсутствует выбранная сессия.", false);
			return;
		}

		final Array<Packet> packets = ArrayFactory.newArray(Packet.class);
		session.getPackets(packets);

		if(packets.isEmpty()) {
			UIUtils.showMessage(uiStage, "Отсутствуют пакеты для сохранения.", false);
			return;
		}

		stage.initOwner(uiStage.getStage());

		File file = fileChooser.showSaveDialog(stage);

		if(file == null) {
			return;
		}

		if(!file.getName().endsWith(NetUtil.SESSION_DUMP_FORMAT)) {
			file = new File(file.getAbsolutePath() + NetUtil.SESSION_DUMP_FORMAT);
		}

		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch(final IOException e) {
				UIUtils.showException(uiStage, e);
				return;
			}
		}

		if(!file.canWrite()) {
			UIUtils.showMessage(uiStage, "В этот фаил нельзя произвести запись.", false);
			return;
		}

		if(packets.size() < 200) {
			NetUtil.save(file, packets);
			return;
		}

		final File saveFile = file;

		final Runnable saveTask = () -> NetUtil.save(saveFile, packets);

		UIUtils.showInfinityProgress(uiStage, saveTask);
	}

	@Override
	public MenuCategory getCategory() {
		return MenuCategory.SESSION;
	}

	@Override
	public MenuItem[] getChildren() {
		return null;
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(SAVE_AS_IMAGE_NAME);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}
}
