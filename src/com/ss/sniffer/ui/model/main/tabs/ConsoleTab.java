package com.ss.sniffer.ui.model.main.tabs;

import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.control.LogView;
import com.ss.sniffer.ui.model.WorkerTab;
import com.ss.sniffer.ui.model.listeners.MessageListener;
import com.ss.sniffer.ui.state.UIState;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import rlib.logging.LoggerListener;
import rlib.logging.LoggerManager;

/**
 * Реализация таба для просмотра ошибок и сообщений программыю
 * 
 * @author Ronn
 */
public class ConsoleTab extends Tab implements WorkerTab, MessageListener, LoggerListener {

	private static final String IMAGE_PATH = "/resource/console_16.png";
	private static final String TAB_NAME = "Консоль";

	private final LogView logView;

	public ConsoleTab() {
		super(TAB_NAME);

		logView = new LogView();

		final VBox root = new VBox();
		root.setAlignment(Pos.CENTER);

		setContent(root);

		final ObservableList<Node> children = root.getChildren();
		children.add(logView);

		logView.prefWidthProperty().bind(root.widthProperty());
		logView.prefHeightProperty().bind(root.heightProperty());

		LoggerManager.addListener(this);
	}

	@Override
	public void addMessage(final String message) {
		logView.addMessage(message);
	}

	@Override
	public void finish() {
	}

	@Override
	public Tab getTab() {
		return this;
	}

	@Override
	public void prepare(final UIState uiStage, final Worker worker) {
		setGraphic(new ImageView(getClass().getResource(IMAGE_PATH).toExternalForm()));
	}

	@Override
	public void println(final String text) {
		addMessage(text);
	}
}
