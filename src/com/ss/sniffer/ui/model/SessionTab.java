package com.ss.sniffer.ui.model;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.ui.state.UIState;

import javafx.scene.control.Tab;

/**
 * Интерфейс для реализации вкладки с функционалом в рамках пакетной сессии.
 * 
 * @author Ronn
 */
public interface SessionTab {

	/**
	 * Завершение работы вкладки.
	 */
	public void finish();

	/**
	 * @return вставляемый таб.
	 */
	public Tab getTab();

	/**
	 * @return можно использовать ли текущую вкладку.
	 */
	public boolean isValid();

	/**
	 * Подготовка таба сессии к работе.
	 * 
	 * @param uiStage UI стадия, в которой проводится подготовка.
	 * @param session сессия, с которой будет работать таб.
	 */
	public void prepare(UIState uiStage, Session session);
}
