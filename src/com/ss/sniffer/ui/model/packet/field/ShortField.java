package com.ss.sniffer.ui.model.packet.field;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class ShortField extends AbstractPacketField<Number> {

	@Override
	public Class<?> getType() {
		return Short.class;
	}

	@Override
	public void read(final ByteBuffer buffer) {
		value = Integer.valueOf(buffer.getShort() & 0xFFFF);
	}
}
