package com.ss.sniffer.ui.model.packet.field;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class ByteField extends AbstractPacketField<Number> {

	@Override
	public Class<?> getType() {
		return Byte.class;
	}

	@Override
	public void read(final ByteBuffer buffer) {
		value = Integer.valueOf(buffer.get() & 0xFF);
	}
}
