package com.ss.sniffer.ui.model.packet;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.field.PacketField;

/**
 * Интерфейс для реализации пакета для работы в UI.
 * 
 * @author Ronn
 */
public interface UIPacket {

	public static final String UNKNOWN_NAME = "<unknown>";

	/**
	 * @return буффер данных пакета.
	 */
	public ByteBuffer getBuffer();

	/**
	 * @return данные пакета.
	 */
	public byte[] getData();

	/**
	 * @return поля пакета.
	 */
	public PacketField[] getFields();

	/**
	 * @return название пакета.
	 */
	public String getName();

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode();

	/**
	 * @return порядок чтения пакета.
	 */
	public ByteOrder getOrder();

	/**
	 * @return сетевой пакет.
	 */
	public Packet getPacket();

	/**
	 * @return время получения.
	 */
	public long getTimestamp();

	/**
	 * @return тип пакета.
	 */
	public PacketType getType();

	/**
	 * @param name новое навание пакета.
	 */
	public void setName(String name);

	/**
	 * @return размер пакета.
	 */
	public int size();
}
