package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * @author Ronn
 */
public class ByteElement extends AbstractDescriptionElement<Integer> {

	private static final String BYTE_IMAGE_NAME = "byte_field_16";

	public ByteElement(final String name, final String byteValue, final Integer value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(BYTE_IMAGE_NAME);
	}
}
