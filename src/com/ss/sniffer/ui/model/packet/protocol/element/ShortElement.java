package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * Элемент структуры представляющий short поле.
 * 
 * @author Ronn
 */
public class ShortElement extends AbstractDescriptionElement<Integer> {

	private static final String IMAGE_NAME = "short_field_16";

	public ShortElement(final String name, final String byteValue, final Integer value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}
}
