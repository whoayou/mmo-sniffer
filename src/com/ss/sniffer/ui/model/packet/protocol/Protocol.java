package com.ss.sniffer.ui.model.packet.protocol;

import com.ss.sniffer.model.PacketModel;

/**
 * Интерфейс для реализации протокола пакетов.
 * 
 * @author Ronn
 */
public interface Protocol {

	/**
	 * Получение описания пакета.
	 * 
	 * @param opcode опкод пакета.
	 * @param server серверный ли пакет.
	 * @return описание пакета.
	 */
	public PacketDescription getDescription(int opcode, boolean server);

	/**
	 * @return название протокола.
	 */
	public String getName();

	/**
	 * @return версия протокола.
	 */
	public String getVersion();

	/**
	 * @param packetModel текущая пакетная модель.
	 * @return доступен ли для текущей модели.
	 */
	public boolean isAvailable(PacketModel packetModel);
}
