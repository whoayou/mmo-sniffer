package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.FloatElement;

import rlib.util.array.Array;

/**
 * @author Ronn
 */
public class FloatBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[4];

		buffer.get(array);
		buffer.position(buffer.position() - 4);

		final float value = buffer.getFloat();

		container.add(new FloatElement(getElementName(), NetUtil.toHEX(array), value));
	}

	@Override
	public String getName() {
		return "float";
	}
}
