package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * Элемент структуры представляющий int поле.
 * 
 * @author Ronn
 */
public class IntegerElement extends AbstractDescriptionElement<Integer> {

	private static final String IMAGE_NAME = "integer_field_16";

	public IntegerElement(final String name, final String byteValue, final Integer value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}
}
