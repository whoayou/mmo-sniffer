package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * Элемент структуры представляющий float поле.
 * 
 * @author Ronn
 */
public class FloatElement extends AbstractDescriptionElement<Float> {

	private static final String IMAGE_NAME = "float_field_16";

	public FloatElement(final String name, final String byteValue, final Float value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}
}
