package com.ss.sniffer.ui.model.packet.protocol;

import javafx.scene.image.Image;

/**
 * Интерфейс для описание элемента описания пакета.
 * 
 * @author Ronn
 */
public interface DescriptionElement {

	/**
	 * @return значение в байтовом виде.
	 */
	public String getByteValue();

	/**
	 * @return внутренние элементы.
	 */
	public DescriptionElement[] getChildren();

	/**
	 * @return иконка элемента.
	 */
	public Image getImage();

	/**
	 * @return название элемента.
	 */
	public String getName();

	/**
	 * @return значение элемента.
	 */
	public <T> T getValue();
}
