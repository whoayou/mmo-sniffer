package com.ss.sniffer.ui.model.packet.protocol;

import java.nio.ByteBuffer;

import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.protocol.builder.ElementBuilder;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Базовая реализация описания структуры пакета.
 * 
 * @author Ronn
 */
public class DefaultPacketDescription implements PacketDescription {

	/** тип пакета */
	private final PacketType type;

	/** билдер элементов структуры пакета */
	private final ElementBuilder builder;

	/** название пакета */
	private final String name;

	/** опкод пакета */
	private final int opcode;

	public DefaultPacketDescription(final ElementBuilder builder, final PacketType type, final String name, final int opcode) {
		this.builder = builder;
		this.type = type;
		this.name = name;
		this.opcode = opcode;
	}

	@Override
	public DescriptionElement[] createElements(final UIPacket uiPacket) {

		final ByteBuffer buffer = uiPacket.getBuffer();
		buffer.position(0);

		final Array<DescriptionElement> container = ArrayFactory.newArray(DescriptionElement.class);

		builder.createElements(buffer, container);

		container.trimToSize();

		return container.array();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getOpcode() {
		return opcode;
	}

	@Override
	public PacketType getType() {
		return type;
	}
}
