package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.LongElement;

import rlib.util.array.Array;

/**
 * @author Ronn
 */
public class LongBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[8];

		buffer.get(array);
		buffer.position(buffer.position() - 8);

		final long value = buffer.getLong();

		container.add(new LongElement(getElementName(), NetUtil.toHEX(array), value));
	}

	@Override
	public String getName() {
		return "long";
	}
}
