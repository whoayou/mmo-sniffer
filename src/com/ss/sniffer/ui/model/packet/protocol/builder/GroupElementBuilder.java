package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.GroupElement;

import rlib.util.StringUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Рутовый строитель элементов пакета.
 * 
 * @author Ronn
 */
public final class GroupElementBuilder extends AbstractElementBuilder {

	private static final String NAME = "groupElements";

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final GroupElement group = new GroupElement(getElementName(), StringUtils.EMPTY, StringUtils.EMPTY);

		container.add(group);

		final Array<DescriptionElement> elements = ArrayFactory.newArray(DescriptionElement.class);

		for(final ElementBuilder builder : children) {
			builder.createElements(buffer, elements);
		}

		for(final DescriptionElement element : elements) {
			group.addChildren(element);
		}
	}

	@Override
	public String getName() {
		return NAME;
	}
}
