package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * Элемент структуры представляющий long поле.
 * 
 * @author Ronn
 */
public class LongElement extends AbstractDescriptionElement<Long> {

	private static final String IMAGE_NAME = "long_field_16";

	public LongElement(final String name, final String byteValue, final Long value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}
}
