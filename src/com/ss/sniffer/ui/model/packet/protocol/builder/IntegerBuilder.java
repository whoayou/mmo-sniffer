package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.IntegerElement;
import com.ss.sniffer.net.util.NetUtil;

import rlib.util.array.Array;

/**
 * @author Ronn
 */
public class IntegerBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[4];

		buffer.get(array);
		buffer.position(buffer.position() - 4);

		final int value = buffer.getInt() & 0xFFFFFFFF;

		container.add(new IntegerElement(getElementName(), NetUtil.toHEX(array), value));
	}

	@Override
	public String getName() {
		return "integer";
	}
}
