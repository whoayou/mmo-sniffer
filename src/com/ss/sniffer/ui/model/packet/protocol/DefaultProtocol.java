package com.ss.sniffer.ui.model.packet.protocol;

import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.packet.PacketType;

import rlib.util.table.IntKey;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

/**
 * Базовая реализация пакетного протокола.
 * 
 * @author Ronn
 */
public class DefaultProtocol implements Protocol {

	/** таблицы описаний пакетов */
	private final Table<IntKey, PacketDescription> clientTable;
	private final Table<IntKey, PacketDescription> serverTable;

	/** название протокола */
	private final String name;
	/** версия протокола */
	private final String version;

	/** класс модели, с которой работает */
	private final Class<PacketModel> modelClass;

	public DefaultProtocol(final Class<PacketModel> modelClass, final String name, final String version) {
		this.clientTable = TableFactory.newIntegerTable();
		this.serverTable = TableFactory.newIntegerTable();
		this.modelClass = modelClass;
		this.name = name;
		this.version = version;
	}

	/**
	 * Добавление описания пакета в протокол.
	 * 
	 * @param description описание пакета.
	 */
	public void addDescription(final PacketDescription description) {
		final Table<IntKey, PacketDescription> table = description.getType() == PacketType.CLIENT_PACKET ? getClientTable() : getServerTable();
		table.put(description.getOpcode(), description);
	}

	private Table<IntKey, PacketDescription> getClientTable() {
		return clientTable;
	}

	@Override
	public PacketDescription getDescription(final int opcode, final boolean server) {
		final Table<IntKey, PacketDescription> table = server ? getServerTable() : getClientTable();

		return table.get(opcode);
	}

	@Override
	public String getName() {
		return name;
	}

	private Table<IntKey, PacketDescription> getServerTable() {
		return serverTable;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public boolean isAvailable(final PacketModel packetModel) {
		return modelClass.isInstance(packetModel);
	}
}
