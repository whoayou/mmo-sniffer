package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.ShortElement;

import rlib.util.array.Array;

/**
 * @author Ronn
 */
public class ShortBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[2];

		buffer.get(array);
		buffer.position(buffer.position() - 2);

		final int value = buffer.getShort() & 0xFFFF;

		container.add(new ShortElement(getElementName(), NetUtil.toHEX(array), value));
	}

	@Override
	public String getName() {
		return "short";
	}
}
