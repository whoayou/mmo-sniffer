package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.CycleElement;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Модель целкического билдера со счетчиком int.
 * 
 * @author Ronn
 */
public class ByteCycleBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[1];

		buffer.get(array);
		buffer.position(buffer.position() - 1);

		final int value = buffer.get() & 0xFF;

		final CycleElement cycle = new CycleElement(getElementName(), NetUtil.toHEX(array), value);

		container.add(cycle);

		final Array<DescriptionElement> elements = ArrayFactory.newArray(DescriptionElement.class);

		final ElementBuilder[] builders = children.array();

		for(int i = 0; i < value; i++) {

			elements.clear();

			for(final ElementBuilder builder : builders) {

				if(builder == null) {
					break;
				}

				builder.createElements(buffer, elements);
			}

			for(final DescriptionElement element : elements) {
				cycle.addChildren(element);
			}
		}
	}

	@Override
	public String getName() {
		return "byteCycle";
	}
}
