package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.element.ByteElement;

import rlib.util.array.Array;

/**
 * @author Ronn
 */
public class ByteBuilder extends AbstractElementBuilder {

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {

		final byte[] array = new byte[1];

		buffer.get(array);
		buffer.position(buffer.position() - 1);

		final int value = buffer.get() & 0xFF;

		container.add(new ByteElement(getElementName(), NetUtil.toHEX(array), value));
	}

	@Override
	public String getName() {
		return "byte";
	}
}
