package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import rlib.util.array.Array;

/**
 * Интерфейс для описания билдера элементов описания пакетов.
 * 
 * @author Ronn
 */
public interface ElementBuilder {

	/**
	 * @param builder билдер внутреннего элемента
	 */
	public void addChildren(ElementBuilder builder);

	/**
	 * Cоздание итогового списка элементов описания пакета.
	 * 
	 * @param buffer буффер с данными.
	 * @param контейнер элементов.
	 */
	public void createElements(ByteBuffer buffer, Array<DescriptionElement> container);

	/**
	 * @return название элемента.
	 */
	public String getElementName();

	/**
	 * @return название билдера.
	 */
	public String getName();

	/**
	 * @param elementName название билдера.
	 */
	public void setElementName(String elementName);
}
