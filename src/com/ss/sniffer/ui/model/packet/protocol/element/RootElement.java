package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import rlib.util.StringUtils;

/**
 * Корневой элемент дерева структуры пакета.
 * 
 * @author Ronn
 */
public final class RootElement extends GroupElement {

	public RootElement(final UIPacket packet, final DescriptionElement[] elements) {
		super(packet.getName(), StringUtils.EMPTY, StringUtils.EMPTY);

		for(final DescriptionElement element : elements) {
			addChildren(element);
		}
	}
}
