package com.ss.sniffer.ui.model.packet;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.ss.sniffer.ui.model.packet.field.PacketField;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.PacketType;

/**
 * Базовая реализация UI пакета.
 * 
 * @author Ronn
 */
public abstract class AbstractUIPacket implements UIPacket {

	/** обернутый пакет */
	protected final Packet packet;

	/** буффер данных пакета */
	protected ByteBuffer buffer;

	/** поля пакета */
	protected PacketField[] fields;

	/** название пакета */
	protected String name;

	/** опкод пакета */
	protected int opcode;

	/** размер пакета */
	protected int size;

	public AbstractUIPacket(final Packet packet) {

		this.packet = packet;

		prepare();

		if(buffer == null) {
			throw new NullPointerException("is not buffer.");
		}
	}

	@Override
	public ByteBuffer getBuffer() {
		return buffer;
	}

	@Override
	public byte[] getData() {
		return buffer.array();
	}

	@Override
	public PacketField[] getFields() {
		return fields;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getOpcode() {
		return opcode;
	}

	@Override
	public ByteOrder getOrder() {
		return buffer.order();
	}

	@Override
	public Packet getPacket() {
		return packet;
	}

	@Override
	public long getTimestamp() {
		return packet.getTimestamp();
	}

	@Override
	public PacketType getType() {
		return packet.getType();
	}

	/**
	 * Подготовка пакета.
	 */
	protected abstract void prepare();

	/**
	 * @param buffer буффер данных.
	 */
	protected void setBuffer(final ByteBuffer buffer) {
		this.buffer = buffer;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param opcode опкод пакета.
	 */
	protected void setOpcode(final int opcode) {
		this.opcode = opcode;
	}

	/**
	 * @param size размер пакета.
	 */
	protected void setSize(final int size) {
		this.size = size;
	}

	@Override
	public int size() {
		return size;
	}
}
