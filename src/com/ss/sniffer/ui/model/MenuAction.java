package com.ss.sniffer.ui.model;

import com.ss.sniffer.model.Worker;

import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;

/**
 * Интерфейс для реализации акшена в меню панели.
 * 
 * @author Ronn
 */
public interface MenuAction {

	/**
	 * Выполнение дочернего акшена.
	 * 
	 * @param item дочерний итем меню.
	 * @param worker работник снифера.
	 */
	public void execute(MenuItem item, Worker worker);

	/**
	 * Выполнить акшен.
	 * 
	 * @param worker работник снифера.
	 */
	public void execute(Worker worker);

	/**
	 * @return категория в меню панели.
	 */
	public MenuCategory getCategory();

	/**
	 * @return внутренние элементы меню.
	 */
	public MenuItem[] getChildren();

	/**
	 * @return изображение для меню.
	 */
	public Image getImage();

	/**
	 * @return название акшена.
	 */
	public String getName();
}
