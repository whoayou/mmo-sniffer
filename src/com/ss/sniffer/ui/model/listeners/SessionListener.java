package com.ss.sniffer.ui.model.listeners;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.session.event.SessionEvent;

/**
 * Интерфейс для реализации слушателя сессии.
 * 
 * @author Ronn
 */
public interface SessionListener {

	/**
	 * Уведомление слушателя о событияя в сессии.
	 * 
	 * @param session сессия, в которой произошло событие.
	 * @param event само событие.
	 */
	public void notify(Session session, SessionEvent event);
}
