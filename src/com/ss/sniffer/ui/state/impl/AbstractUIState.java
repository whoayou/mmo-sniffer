package com.ss.sniffer.ui.state.impl;

import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import com.ss.sniffer.ui.state.UIState;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;

/**
 * Базовая реализация UI состояния.
 * 
 * @author Ronn
 */
public abstract class AbstractUIState implements UIState {

	protected static final Logger LOGGER = LoggerManager.getLogger(UIState.class);

	/** обработчик события диалогового окна */
	private final EventHandler<WindowEvent> messageDialogHandler = event -> enable();

	/** текущая FX стадия */
	private Stage currentStage;

	@Override
	public void disable() {

		final Stage stage = getCurrentStage();

		if(stage == null || !stage.isShowing()) {
			return;
		}

		stage.hide();
	}

	@Override
	public void enable() {

		final Stage stage = getCurrentStage();

		if(stage == null || stage.isShowing()) {
			return;
		}

		stage.show();
	}

	protected Stage getCurrentStage() {
		return currentStage;
	}

	@Override
	public EventHandler<WindowEvent> getMessageDialogHandler() {
		return messageDialogHandler;
	}

	@Override
	public Stage getStage() {
		return currentStage;
	}

	@Override
	public void gotoNext(final UIState uiStage) {
		uiStage.start(getCurrentStage());
		stop();
	}

	protected void setCurrentStage(final Stage currentStage) {
		this.currentStage = currentStage;
	}

	@Override
	public void start(final Stage stage) {
		setCurrentStage(stage);
	}

	@Override
	public void stop() {
		setCurrentStage(null);
	}
}
