package com.ss.sniffer.ui.state.impl;

import com.ss.sniffer.Config;
import com.ss.sniffer.ui.Constans;
import com.ss.sniffer.ui.uistage.load.LoadTask;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Стадия инициализация приложения.
 * 
 * @author Ronn
 */
public class LoadUIState extends AbstractUIState {

	private static final String INDICATOR_LABEL = "Инициализация программы...";

	private static final int INDICATOR_SIZE = 100;
	private static final int STAGE_HEIGHT = 160;
	private static final int STAGE_WIDTH = 450;

	private final ConfigUIState configStage;

	private Scene scene;

	public LoadUIState() {
		this.configStage = new ConfigUIState();
	}

	/**
	 * Завершение инициализации.
	 */
	public void finish() {
		gotoNext(configStage);
	}

	@Override
	public void start(final Stage stage) {
		super.start(stage);

		final VBox root = new VBox();
		root.setAlignment(Pos.CENTER);

		scene = new Scene(root, STAGE_WIDTH, STAGE_HEIGHT, Color.WHITESMOKE);

		final ProgressIndicator indicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
		indicator.setMinWidth(INDICATOR_SIZE);
		indicator.setMinHeight(INDICATOR_SIZE);

		final Label label = new Label(INDICATOR_LABEL);
		label.setAlignment(Pos.CENTER);
		label.setMinHeight(Config.MIN_ELEMENT_HEIGHT);

		root.getChildren().addAll(label, indicator);

		VBox.setMargin(label, Constans.INSETS_5);

		stage.setScene(scene);
		stage.show();
		stage.setTitle(Config.TITLE);
		stage.centerOnScreen();
		stage.setResizable(false);

		final Thread loadThread = new Thread(new LoadTask(this), "Initialized Thread");
		loadThread.start();
	}

	@Override
	public void stop() {
		super.stop();
		scene = null;
	}
}
