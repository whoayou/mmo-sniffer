package com.ss.sniffer.ui.state;

import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Интерфейс для реализации состояния UI.
 * 
 * @author Ronn
 */
public interface UIState {

	/**
	 * Выключить.
	 */
	public void disable();

	/**
	 * Включить.
	 */
	public void enable();

	/**
	 * @return обработчик закрытия диалога с сообщением.
	 */
	public EventHandler<WindowEvent> getMessageDialogHandler();

	/**
	 * @return текущая стадия.
	 */
	public Stage getStage();

	/**
	 * Переход к следующему состоянию.
	 */
	public void gotoNext(UIState state);

	/**
	 * Активация стадии.
	 * 
	 * @param stage стадия FX, на которой показать UI.
	 */
	public void start(Stage stage);

	/**
	 * Завершение стадии.
	 */
	public void stop();
}
