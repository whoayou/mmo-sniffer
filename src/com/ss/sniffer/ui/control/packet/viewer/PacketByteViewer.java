package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.Config;
import com.ss.sniffer.Start;
import com.ss.sniffer.model.Session;
import com.ss.sniffer.ui.Constans;
import com.ss.sniffer.ui.model.packet.UIPacket;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import rlib.util.StringUtils;
import rlib.util.Util;

import java.nio.ByteBuffer;

/**
 * Реализация просмоторщика пакетов.
 * 
 * @author Ronn
 */
public class PacketByteViewer extends VBox implements PacketViewer {

	private static final String CONVERT_LABEL_NAME = "Конвертировать:";

	private static final int FIELD_WIDTH = 150;
	private static final int LABEL_ICON_SIZE = 23;

	private static Image shortFieldImage;
	private static Image longFieldImage;
	private static Image floatFieldImage;
	private static Image integerFieldImage;

	public static Image getFloatFieldImage() {

		if(floatFieldImage == null) {
			floatFieldImage = new Image(Start.class.getResourceAsStream("/resource/float_field_16.png"));
		}

		return floatFieldImage;
	}

	public static Image getIntegerFieldImage() {

		if(integerFieldImage == null) {
			integerFieldImage = new Image(Start.class.getResourceAsStream("/resource/integer_field_16.png"));
		}

		return integerFieldImage;
	}

	public static Image getLongFieldImage() {

		if(longFieldImage == null) {
			longFieldImage = new Image(Start.class.getResourceAsStream("/resource/long_field_16.png"));
		}

		return longFieldImage;
	}

	public static Image getShortFieldImage() {

		if(shortFieldImage == null) {
			shortFieldImage = new Image(Start.class.getResourceAsStream("/resource/short_field_16.png"));
		}

		return shortFieldImage;
	}

	private final TextArea byteArea;

	private final TextField longField;
	private final TextField integerField;
	private final TextField shortField;
	private final TextField floatField;
	private final TextField convertField;

	private final Label longLabel;
	private final Label integerLabel;
	private final Label shortLabel;
	private final Label floatLabel;
	private final Label convertLabel;

	private final ByteBuffer buffer;

	public PacketByteViewer() {

		byteArea = new TextArea();
		byteArea.setId("byte_packet_view");
		byteArea.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

			String selectedText = byteArea.getSelectedText();
			selectedText = selectedText.replaceAll(" ", StringUtils.EMPTY);

			getConvertField().setText(selectedText);
			convert();
		});

		longField = new TextField();
		longField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		longField.setMinWidth(FIELD_WIDTH);
		longField.setAlignment(Pos.CENTER_RIGHT);
		longField.setEditable(false);
		longField.prefWidthProperty().bind(byteArea.widthProperty().divide(2));

		integerField = new TextField();
		integerField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		integerField.setMinWidth(FIELD_WIDTH);
		integerField.setAlignment(Pos.CENTER_RIGHT);
		integerField.setEditable(false);
		integerField.prefWidthProperty().bind(byteArea.widthProperty().divide(2));

		shortField = new TextField();
		shortField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		shortField.setMinWidth(FIELD_WIDTH);
		shortField.setAlignment(Pos.CENTER_RIGHT);
		shortField.setEditable(false);
		shortField.prefWidthProperty().bind(byteArea.widthProperty().divide(2));

		floatField = new TextField();
		floatField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		floatField.setMinWidth(FIELD_WIDTH);
		floatField.setAlignment(Pos.CENTER_RIGHT);
		floatField.setEditable(false);
		floatField.prefWidthProperty().bind(byteArea.widthProperty().divide(2));

		longLabel = new Label(StringUtils.EMPTY, new ImageView(getLongFieldImage()));
		longLabel.setMinHeight(LABEL_ICON_SIZE);
		longLabel.setMaxHeight(LABEL_ICON_SIZE);
		longLabel.setMinWidth(LABEL_ICON_SIZE);
		longLabel.setMaxWidth(LABEL_ICON_SIZE);
		longLabel.setAlignment(Pos.CENTER);

		integerLabel = new Label(StringUtils.EMPTY, new ImageView(getIntegerFieldImage()));
		integerLabel.setMinHeight(LABEL_ICON_SIZE);
		integerLabel.setMaxHeight(LABEL_ICON_SIZE);
		integerLabel.setMinWidth(LABEL_ICON_SIZE);
		integerLabel.setMaxWidth(LABEL_ICON_SIZE);
		integerLabel.setAlignment(Pos.CENTER);

		shortLabel = new Label(StringUtils.EMPTY, new ImageView(getShortFieldImage()));
		shortLabel.setMinHeight(LABEL_ICON_SIZE);
		shortLabel.setMaxHeight(LABEL_ICON_SIZE);
		shortLabel.setMinWidth(LABEL_ICON_SIZE);
		shortLabel.setMaxWidth(LABEL_ICON_SIZE);
		shortLabel.setAlignment(Pos.CENTER);

		floatLabel = new Label(StringUtils.EMPTY, new ImageView(getFloatFieldImage()));
		floatLabel.setMinHeight(LABEL_ICON_SIZE);
		floatLabel.setMaxHeight(LABEL_ICON_SIZE);
		floatLabel.setMinWidth(LABEL_ICON_SIZE);
		floatLabel.setMaxWidth(LABEL_ICON_SIZE);
		floatLabel.setAlignment(Pos.CENTER);

		convertLabel = new Label(CONVERT_LABEL_NAME);
		convertLabel.setAlignment(Pos.CENTER_RIGHT);
		convertLabel.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		convertLabel.setMinWidth(140);

		convertField = new TextField();
		convertField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		convertField.prefWidthProperty().bind(byteArea.widthProperty());
		convertField.addEventHandler(KeyEvent.KEY_RELEASED, event -> convert());

		buffer = ByteBuffer.allocate(8);

		final ObservableList<Node> children = getChildren();

		VBox.setMargin(byteArea, Constans.INSETS_5);

		children.add(byteArea);

		HBox hBox = new HBox(2);
		hBox.setAlignment(Pos.CENTER);
		hBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		hBox.getChildren().addAll(convertLabel, convertField);

		VBox.setMargin(hBox, Constans.INSETS_5);

		children.add(hBox);

		hBox = new HBox(4);
		hBox.setAlignment(Pos.CENTER);
		hBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		hBox.getChildren().addAll(shortLabel, shortField, integerLabel, integerField);

		VBox.setMargin(hBox, Constans.INSETS_5);

		children.add(hBox);

		hBox = new HBox(4);
		hBox.setAlignment(Pos.CENTER);
		hBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		hBox.getChildren().addAll(longLabel, longField, floatLabel, floatField);

		VBox.setMargin(hBox, Constans.INSETS_5);

		children.add(hBox);

		byteArea.prefHeightProperty().bind(heightProperty());
	}

    public TextField getConvertField() {
        return convertField;
    }

    /**
	 * Конвертирование байтов в данные.
	 */
	private void convert() {

		convertClear();

		final ByteBuffer buffer = getBuffer();

		buffer.clear();
		try {

			final String converText = convertField.getText();

			for(int i = 0, length = converText.length() - 1; i <= length && buffer.hasRemaining(); i++) {

				final char first = converText.charAt(i);

				if(i == length) {
					buffer.put((byte) Integer.parseInt(String.valueOf(first), 16));
				} else {

					final char second = converText.charAt(++i);

					buffer.put((byte) Integer.parseInt(first + StringUtils.EMPTY + second, 16));
				}
			}

			buffer.flip();

			if(buffer.limit() > 1) {
				shortField.setText(String.valueOf(buffer.getShort() & 0xFFFF));
				buffer.position(0);
			}

			if(buffer.limit() > 3) {
				integerField.setText(String.valueOf(buffer.getInt() & 0xFFFFFFFF));
				buffer.position(0);
				floatField.setText(String.valueOf(buffer.getFloat()));
				buffer.position(0);
			}

			if(buffer.limit() > 7) {
				longField.setText(String.valueOf(buffer.getLong()));
			}
		} catch(final NumberFormatException e) {
		} finally {
			buffer.clear();
		}
	}

	private void convertClear() {
		longField.setText(StringUtils.EMPTY);
		shortField.setText(StringUtils.EMPTY);
		integerField.setText(StringUtils.EMPTY);
		floatField.setText(StringUtils.EMPTY);
	}

	private ByteBuffer getBuffer() {
		return buffer;
	}

	@Override
	public void setPacket(final Session session, final UIPacket uiPacket) {

		if(uiPacket == null) {
			byteArea.setText(StringUtils.EMPTY);
		} else {

			buffer.clear();
			buffer.order(uiPacket.getOrder());

			final byte[] data = uiPacket.getData();

			byteArea.setText(Util.hexdump(data, data.length));
		}

		convertField.setText(StringUtils.EMPTY);

		convertClear();
	}
}
