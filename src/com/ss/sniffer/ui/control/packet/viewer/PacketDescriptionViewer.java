package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;
import com.ss.sniffer.ui.model.packet.protocol.PacketDescription;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;
import com.ss.sniffer.ui.model.packet.protocol.element.RootElement;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;

/**
 * Реализация структурного просмоторщика пакетов.
 * 
 * @author Ronn
 */
public class PacketDescriptionViewer extends VBox implements PacketViewer {

	/** деверо структуры элементов пакета */
	private final TreeView<DescriptionElement> structureView;

	public PacketDescriptionViewer() {

		structureView = new TreeView<DescriptionElement>();
		structureView.setCellFactory(ElementCell.FACTORY);

		final ObservableList<Node> children = getChildren();

		children.add(structureView);

		structureView.prefHeightProperty().bind(heightProperty());
	}

	@Override
	public void setPacket(final Session session, final UIPacket uiPacket) {

		final Protocol protocol = session.getProtocol();

		if(protocol == null) {
			structureView.setRoot(null);
			return;
		}

		final PacketDescription description = protocol.getDescription(uiPacket.getOpcode(), uiPacket.getType() == PacketType.SERVER_PACKET);

		if(description == null) {
			structureView.setRoot(null);
			return;
		}

		final DescriptionElement input = new RootElement(uiPacket, description.createElements(uiPacket));

		final TreeElement inputItem = new TreeElement(input);

		structureView.setShowRoot(true);
		structureView.setRoot(inputItem);

		inputItem.setExpanded(true);
	}
}
