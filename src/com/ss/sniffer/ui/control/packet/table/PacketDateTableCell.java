package com.ss.sniffer.ui.control.packet.table;

import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Реализация ячейки таблици с отображением времени получения пакета.
 * 
 * @author Ronn
 */
public class PacketDateTableCell extends TableCell<PacketItem, Long> {

	/** формат даты */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("mm:ss:SSS");

	/** контейнер даты для формата */
	private static final Date DATE = new Date();

	public static final Callback<TableColumn<PacketItem, Long>, TableCell<PacketItem, Long>> FACTORY = param -> new PacketDateTableCell();

	@Override
	protected void updateItem(final Long item, final boolean empty) {

		if(item == null) {
			setText(StringUtils.EMPTY);
			return;
		}

		synchronized(DATE_FORMAT) {
			DATE.setTime(item);
			setText(DATE_FORMAT.format(DATE));
		}

		setAlignment(Pos.CENTER);
	}
}
