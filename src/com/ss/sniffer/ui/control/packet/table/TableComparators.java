package com.ss.sniffer.ui.control.packet.table;

import java.util.Comparator;

import com.ss.sniffer.net.packet.PacketType;

/**
 * @author Ronn
 */
public interface TableComparators {

	public static final Comparator<PacketType> PACKET_TYPE_COMPARATOR = (first, second) -> first.ordinal() - second.ordinal();

	public static final Comparator<String> PACKET_NAME_COMPARATOR = (first, second) -> first.compareToIgnoreCase(second);

	public static final Comparator<Long> PACKET_DATE_COMPARATOR = (first, second) -> (int) (first - second);

	public static final Comparator<Integer> PACKET_OPCODE_OR_SIZE_COMPARATOR = (first, second) -> first - second;
}
