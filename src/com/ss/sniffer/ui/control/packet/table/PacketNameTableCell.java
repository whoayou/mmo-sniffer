package com.ss.sniffer.ui.control.packet.table;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Реализация ячейки таблици с отображением имени пакета.
 * 
 * @author Ronn
 */
public class PacketNameTableCell extends TableCell<PacketItem, String> {

	public static final Callback<TableColumn<PacketItem, String>, TableCell<PacketItem, String>> FACTORY = param -> new PacketNameTableCell();

	@Override
	protected void updateItem(final String item, final boolean empty) {

		if(item == null) {
			setText(StringUtils.EMPTY);
			return;
		}

		setText(item);
		setAlignment(Pos.CENTER);
	}
}
