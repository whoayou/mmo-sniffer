package com.ss.sniffer.ui.control.packet.table;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Реализация ячейки таблици с отображением размера пакета.
 * 
 * @author Ronn
 */
public class PacketSizeTableCell extends TableCell<PacketItem, Integer> {

	public static final Callback<TableColumn<PacketItem, Integer>, TableCell<PacketItem, Integer>> FACTORY = param -> new PacketSizeTableCell();

	@Override
	protected void updateItem(final Integer item, final boolean empty) {

		if(item == null) {
			setText(StringUtils.EMPTY);
			return;
		}

		setText(item + "b");
		setAlignment(Pos.CENTER);
	}
}
