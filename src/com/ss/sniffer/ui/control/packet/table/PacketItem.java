package com.ss.sniffer.ui.control.packet.table;

import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.net.packet.PacketType;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Реализация пакета итема для пакетной таблицы.
 * 
 * @author Ronn
 */
public final class PacketItem {

	/** тип пакета */
	private final SimpleObjectProperty<PacketType> type;

	/** название пакета */
	private final SimpleStringProperty name;
	/** дата получения пакета */
	private final SimpleLongProperty date;
	/** опкод пакета */
	private final SimpleIntegerProperty opcode;
	/** размер пакета */
	private final SimpleIntegerProperty size;

	/** UI пакет */
	private UIPacket packet;

	public PacketItem() {
		this.type = new SimpleObjectProperty<>();
		this.name = new SimpleStringProperty();
		this.opcode = new SimpleIntegerProperty();
		this.size = new SimpleIntegerProperty();
		this.date = new SimpleLongProperty();
	}

	public SimpleLongProperty dateProperty() {
		return date;
	}

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode() {
		return packet == null ? 0 : packet.getOpcode();
	}

	/**
	 * @return отображаемый пакет.
	 */
	public UIPacket getPacket() {
		return packet;
	}

	/**
	 * @return тип пакета.
	 */
	public PacketType getType() {
		return packet.getType();
	}

	public SimpleStringProperty nameProperty() {
		return name;
	}

	public SimpleIntegerProperty opcodeProperty() {
		return opcode;
	}

	/**
	 * Установка отображаемого пакета.
	 * 
	 * @param packet отображаемый пакет.
	 */
	public void setPacket(final UIPacket packet) {
		type.set(packet.getType());
		name.set(packet.getName());
		date.set(packet.getTimestamp());
		opcode.set(packet.getOpcode());
		size.set(packet.size());
		this.packet = packet;
	}

	public SimpleIntegerProperty sizeProperty() {
		return size;
	}

	@Override
	public String toString() {
		return String.valueOf(packet);
	}

	public SimpleObjectProperty<PacketType> typeProperty() {
		return type;
	}
}
