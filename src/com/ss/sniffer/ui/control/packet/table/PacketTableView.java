package com.ss.sniffer.ui.control.packet.table;

import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.net.packet.PacketType;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Реализация таблицы пакетов.
 * 
 * @author Ronn
 */
public class PacketTableView extends TableView<PacketItem> {

	private static final int PACKET_DATE_COLUMN_WIDTH = 100;
	private static final int PACKET_SIZE_COLUMN_WIDTH = 60;
	private static final int PACKET_NAME_COLUMN_WIDTH = 100;
	private static final int PACKET_OPCODE_COLUMN_WIDTH = 100;
	private static final int PACKET_TYPE_COLUMN_WIDTH = 40;

	private static final String DATE_FIELD = "date";
	private static final String SIZE_FIELD = "size";
	private static final String NAME_FIELD = "name";
	private static final String OPCODE_FIELD = "opcode";
	private static final String TYPE_FIELD = "type";

	private final boolean unique;

	public PacketTableView(final boolean unique, final ChangeListener<PacketItem> selectionListener) {
		this.unique = unique;

		setEditable(false);

		final TableViewSelectionModel<PacketItem> selectionModel = getSelectionModel();

		selectionModel.setSelectionMode(SelectionMode.SINGLE);
		selectionModel.selectedItemProperty().addListener(selectionListener);

		final ObservableList<PacketItem> items = FXCollections.observableArrayList();

		setItems(items);

		final TableColumn<PacketItem, PacketType> packetTypeColumn = new TableColumn<PacketItem, PacketType>("Тип");
		packetTypeColumn.setCellValueFactory(new PropertyValueFactory<PacketItem, PacketType>(TYPE_FIELD));
		packetTypeColumn.setCellFactory(PacketTypeTableCell.FACTORY);
		packetTypeColumn.setComparator(TableComparators.PACKET_TYPE_COMPARATOR);
		packetTypeColumn.setMinWidth(PACKET_TYPE_COLUMN_WIDTH);
		packetTypeColumn.setMaxWidth(PACKET_TYPE_COLUMN_WIDTH);

		final TableColumn<PacketItem, Integer> packetOpcodeColumn = new TableColumn<PacketItem, Integer>("Опкод");
		packetOpcodeColumn.setCellValueFactory(new PropertyValueFactory<PacketItem, Integer>(OPCODE_FIELD));
		packetOpcodeColumn.setCellFactory(PacketOpcodeTableCell.FACTORY);
		packetOpcodeColumn.setComparator(TableComparators.PACKET_OPCODE_OR_SIZE_COMPARATOR);
		packetOpcodeColumn.setMinWidth(PACKET_OPCODE_COLUMN_WIDTH);
		packetOpcodeColumn.setMaxWidth(PACKET_OPCODE_COLUMN_WIDTH);

		final TableColumn<PacketItem, String> packetNameColumn = new TableColumn<PacketItem, String>("Название");
		packetNameColumn.setCellValueFactory(new PropertyValueFactory<PacketItem, String>(NAME_FIELD));
		packetNameColumn.setCellFactory(PacketNameTableCell.FACTORY);
		packetNameColumn.setComparator(TableComparators.PACKET_NAME_COMPARATOR);
		packetNameColumn.setMinWidth(PACKET_NAME_COLUMN_WIDTH);

		final TableColumn<PacketItem, Integer> packetSizeColumn = new TableColumn<PacketItem, Integer>("Размер");
		packetSizeColumn.setCellValueFactory(new PropertyValueFactory<PacketItem, Integer>(SIZE_FIELD));
		packetSizeColumn.setCellFactory(PacketSizeTableCell.FACTORY);
		packetSizeColumn.setComparator(TableComparators.PACKET_OPCODE_OR_SIZE_COMPARATOR);
		packetSizeColumn.setMinWidth(PACKET_SIZE_COLUMN_WIDTH);
		packetSizeColumn.setMaxWidth(PACKET_SIZE_COLUMN_WIDTH);

		final TableColumn<PacketItem, Long> packetDateColumn = new TableColumn<PacketItem, Long>("Дата");
		packetDateColumn.setCellValueFactory(new PropertyValueFactory<PacketItem, Long>(DATE_FIELD));
		packetDateColumn.setCellFactory(PacketDateTableCell.FACTORY);
		packetDateColumn.setComparator(TableComparators.PACKET_DATE_COMPARATOR);
		packetDateColumn.setMinWidth(PACKET_DATE_COLUMN_WIDTH);
		packetDateColumn.setMaxWidth(PACKET_DATE_COLUMN_WIDTH);

		final ObservableList<TableColumn<PacketItem, ?>> columns = getColumns();
		columns.add(packetTypeColumn);
		columns.add(packetOpcodeColumn);
		columns.add(packetNameColumn);
		columns.add(packetSizeColumn);
		columns.add(packetDateColumn);
	}

	/**
	 * Добавить либо обновить в таблице пакет.
	 * 
	 * @param packet новый UI пакет.
	 */
	public void addPacket(final UIPacket packet) {

		final ObservableList<PacketItem> items = getItems();

		if(!isUnique()) {
			final PacketItem item = new PacketItem();
			item.setPacket(packet);
			items.add(item);
		} else {

			final int opcode = packet.getOpcode();

			for(final PacketItem item : items) {
				if(item.getType() == packet.getType() && item.getOpcode() == opcode) {
					item.setPacket(packet);
					return;
				}
			}

			final PacketItem item = new PacketItem();
			item.setPacket(packet);
			items.add(item);
		}
	}

	/**
	 * @return уникальные ли здесь пакеты.
	 */
	public boolean isUnique() {
		return unique;
	}
}
