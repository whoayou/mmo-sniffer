package com.ss.sniffer.ui.control;

import javafx.scene.control.TextArea;
import rlib.util.StringUtils;

/**
 * Реализация отображения лога работы.
 * 
 * @author Ronn
 */
public class LogView extends TextArea {

	/** контейнер текста */
	private final StringBuilder text;

	public LogView() {
		this.text = new StringBuilder();

		setStyle("  -fx-text-fill: red");
		setWrapText(true);
		setEditable(false);
	}

	/**
	 * Добавление сообщения.
	 * 
	 * @param message добавляемое сообщение.
	 */
	public void addMessage(final String message) {

		if(StringUtils.isEmpty(message)) {
			return;
		}

		text.append(message).append('\n');
		setText(text.toString());
	}
}
