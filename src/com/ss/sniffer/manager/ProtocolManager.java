package com.ss.sniffer.manager;

import java.io.File;

import com.ss.sniffer.Config;
import com.ss.sniffer.document.ProtocolDocument;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;
import com.ss.sniffer.util.Utils;

import rlib.manager.InitializeManager;
import rlib.util.FileUtils;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Менеджер по подгрузке протоколов.
 * 
 * @author Ronn
 */
public class ProtocolManager {

	private static ProtocolManager instance;

	public static ProtocolManager getInstance() {

		if(instance == null) {
			instance = new ProtocolManager();
		}

		return instance;
	}

	/** список протоколов */
	private final Array<Protocol> protocols;

	public ProtocolManager() {

		InitializeManager.valid(getClass());

		this.protocols = ArrayFactory.newArray(Protocol.class);

		File protocolFolder = Utils.getRootFolderFromClass(Config.ROOT_CLASS);
		protocolFolder = new File(protocolFolder.getAbsolutePath() + File.separator + "protocols");

		if(!protocolFolder.exists() || protocolFolder.isFile()) {
			protocolFolder.mkdir();
		}

		final File[] files = FileUtils.getFiles(protocolFolder, ".xml");

		for(final File file : files) {
			protocols.addAll(new ProtocolDocument(file).parse());
		}
	}

	/**
	 * @return список доступных протоколов для текущей модели.
	 */
	public Array<Protocol> getAvailableProtocols() {

		final Array<Protocol> protocols = getProtocols();
		final Array<Protocol> result = ArrayFactory.newArray(Protocol.class);

		for(final Protocol protocol : protocols) {
			if(protocol.isAvailable(Config.PACKET_MODEL)) {
				result.add(protocol);
			}
		}

		return result;
	}

	private Array<Protocol> getProtocols() {
		return protocols;
	}
}
