package com.ss.sniffer.manager;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.SessionTab;
import com.ss.sniffer.ui.model.WorkerTab;
import com.ss.sniffer.ui.model.packet.field.PacketField;
import com.ss.sniffer.ui.model.packet.protocol.builder.ElementBuilder;
import com.ss.sniffer.ui.state.UIState;

import rlib.classpath.ClassPathScaner;
import rlib.classpath.ClassPathScannerFactory;
import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Менеджр по сканированию classpath.
 * 
 * @author Ronn
 */
public class ClassManager {

	private static ClassManager instance;

	public static ClassManager getInstance() {

		if(instance == null) {
			instance = new ClassManager();
		}

		return instance;
	}

	/** сканнер доступных классов */
	private final ClassPathScaner scaner;

	public ClassManager() {

		InitializeManager.valid(getClass());

		scaner = ClassPathScannerFactory.newManifestScanner(Config.ROOT_CLASS, "JavaFX-Class-Path");
		scaner.scanning();
	}

	/**
	 * @return список всех доступных классов билдеров элементов описания пакета.
	 */
	public Array<Class<ElementBuilder>> getAllBuilders() {

		final Array<Class<ElementBuilder>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, ElementBuilder.class);

		return classes;
	}

	/**
	 * Получение всех главных вкладок дял работы.
	 * 
	 * @param uiStage UI стадия.
	 * @return список доступных главных вкладок.
	 */
	public Array<WorkerTab> getAllMainTabs(final UIState uiStage) {

		final Array<Class<WorkerTab>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, WorkerTab.class);

		final Array<WorkerTab> actions = ArrayFactory.newArray(WorkerTab.class, classes.size());

		for(final Class<WorkerTab> cs : classes) {
			try {
				actions.add(cs.newInstance());
			} catch(InstantiationException | IllegalAccessException e) {
				UIUtils.showMessage(uiStage, "Проблема с созданием главной вкладки " + cs, true);
			}
		}

		return actions;
	}

	/**
	 * Получение всех акшенов для меню панели.
	 * 
	 * @param uiStage UI стадия.
	 * @return список доступных акшенов.
	 */
	public Array<MenuAction> getAllMenuActions(final UIState uiStage) {

		final Array<Class<MenuAction>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, MenuAction.class);

		final Array<MenuAction> actions = ArrayFactory.newArray(MenuAction.class, classes.size());

		for(final Class<MenuAction> cs : classes) {
			try {
				actions.add(cs.newInstance());
			} catch(InstantiationException | IllegalAccessException e) {
				UIUtils.showMessage(uiStage, "Проблема с созданием акшена меню " + cs, true);
			}
		}

		return actions;
	}

	/**
	 * @return список доступных классов пакетных полей.
	 */
	public Array<Class<PacketField>> getAllPacketFields() {

		final Array<Class<PacketField>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, PacketField.class);

		return classes;
	}

	/**
	 * Получение всех доступных вкладок для работы в сессии.
	 * 
	 * @param uiStage текущая UI стадия.
	 * @return список доступных вкладок.
	 */
	public Array<SessionTab> getAllSessionTabs(final UIState uiStage) {

		final Array<Class<SessionTab>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, SessionTab.class);

		final Array<SessionTab> tabs = ArrayFactory.newArray(SessionTab.class, classes.size());

		for(final Class<SessionTab> cs : classes) {
			try {
				final SessionTab sessionTab = cs.newInstance();

				if(sessionTab.isValid()) {
					tabs.add(sessionTab);
				}
			} catch(InstantiationException | IllegalAccessException e) {
				UIUtils.showMessage(uiStage, "Проблема с созданием вкладки сессии " + cs, true);
			}
		}

		return tabs;
	}

	/**
	 * Получение списка доступных крипторов.
	 * 
	 * @param uiStage стадия UI, из которой вызывается.
	 * @return список крипторов.
	 */
	public Array<CryptType> getCryptTypes(final UIState uiStage) {

		final Array<Class<CryptType>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, CryptType.class);

		final Array<CryptType> crypts = ArrayFactory.newArray(CryptType.class, classes.size());

		for(final Class<CryptType> cs : classes) {
			try {
				crypts.add(cs.newInstance());
			} catch(InstantiationException | IllegalAccessException e) {
				UIUtils.showMessage(uiStage, "Проблема с созданием криптора " + cs, true);
			}
		}

		return crypts;
	}

	/**
	 * Получение списка доступных пакетных моделей.
	 * 
	 * @param uiStage стадия UI, из которой вызывается.
	 * @return список пакетных моделей.
	 */
	public Array<PacketModel> getPacketModels(final UIState uiStage) {

		final Array<Class<PacketModel>> classes = ArrayFactory.newArray(Class.class);

		scaner.findImplements(classes, PacketModel.class);

		final Array<PacketModel> models = ArrayFactory.newArray(PacketModel.class, classes.size());

		for(final Class<PacketModel> cs : classes) {
			try {
				models.add(cs.newInstance());
			} catch(InstantiationException | IllegalAccessException e) {
				UIUtils.showMessage(uiStage, "Проблема с созданием модели " + cs, true);
			}
		}

		return models;
	}
}
