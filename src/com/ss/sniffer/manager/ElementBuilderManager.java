package com.ss.sniffer.manager;

import com.ss.sniffer.ui.model.packet.protocol.builder.ElementBuilder;

import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

/**
 * Менеджер по по хранению всех видов билдеров полей элементов структуры
 * пакетов.
 * 
 * @author Ronn
 */
public class ElementBuilderManager {

	private static ElementBuilderManager instance;

	public static ElementBuilderManager getInstance() {

		if(instance == null) {
			instance = new ElementBuilderManager();
		}

		return instance;
	}

	/** таблица классов билдеров */
	private final Table<String, Class<? extends ElementBuilder>> table;

	public ElementBuilderManager() {

		InitializeManager.valid(getClass());

		this.table = TableFactory.newObjectTable();

		final ClassManager manager = ClassManager.getInstance();

		final Array<Class<ElementBuilder>> builders = manager.getAllBuilders();

		for(final Class<ElementBuilder> cs : builders) {

			try {

				final ElementBuilder builder = cs.newInstance();

				table.put(builder.getName(), cs);
			} catch(InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Получение билдера элемента пакета по его имени.
	 * 
	 * @param name название билдера.
	 * @return билдер элемента.
	 */
	public ElementBuilder getBuilder(final String name) {

		final Class<? extends ElementBuilder> cs = table.get(name);

		if(cs == null) {
			return null;
		}

		try {
			return cs.newInstance();
		} catch(InstantiationException | IllegalAccessException e) {
			return null;
		}
	}
}
