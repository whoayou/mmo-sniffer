package com.ss.sniffer.manager;

import com.ss.sniffer.ui.model.packet.field.PacketField;

import rlib.manager.InitializeManager;
import rlib.util.array.Array;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

/**
 * Менеджер сбора и хранения доступных видов пакетных полей.
 * 
 * @author Ronn
 */
public class PacketFieldManager {

	private static PacketFieldManager instance;

	public static PacketFieldManager getInstance() {

		if(instance == null) {
			instance = new PacketFieldManager();
		}

		return instance;
	}

	/** таблица всех классов пакетных полей */
	private final Table<Class<?>, Class<PacketField>> table;

	private PacketFieldManager() {

		InitializeManager.valid(getClass());

		this.table = TableFactory.newObjectTable();

		final ClassManager classManager = ClassManager.getInstance();

		final Array<Class<PacketField>> classes = classManager.getAllPacketFields();

		for(final Class<PacketField> fieldClass : classes) {
			try {
				final PacketField field = fieldClass.newInstance();

				table.put(field.getType(), fieldClass);
			} catch(InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Создание пакетного поля для указанного типа данных.
	 * 
	 * @param type тип данных.
	 * @return пакетное поле для этого типа.
	 */
	public PacketField createField(final Class<?> type) {

		final Class<PacketField> fieldClass = table.get(type);

		try {
			return fieldClass.newInstance();
		} catch(InstantiationException | IllegalAccessException e) {
			return null;
		}
	}
}
