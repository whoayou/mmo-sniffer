package com.ss.sniffer.manager;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import rlib.manager.InitializeManager;

/**
 * Менеджер исполнения заданий.
 * 
 * @author Ronn
 */
public final class ExecutorManager {

	private static ExecutorManager instance;

	public static final ExecutorManager getInstance() {

		if(instance == null) {
			instance = new ExecutorManager();
		}

		return instance;
	}

	/** исполнитель заданий */
	private final ExecutorService executor;

	/** исполнитель отложенных заданий */
	private final ScheduledExecutorService scheduler;

	public ExecutorManager() {

		InitializeManager.valid(getClass());

		this.executor = Executors.newFixedThreadPool(2);
		this.scheduler = Executors.newScheduledThreadPool(2);
	}

	/**
	 * @param task выполняемое задание.
	 */
	public void execute(final Runnable task) {
		executor.execute(task);
	}

	/**
	 * @param task выполняемое задание.
	 * @param delay задержка до выполнения.
	 * @return ссылка на ожидающее задание.
	 */
	public ScheduledFuture<?> schedule(final Runnable task, final int delay) {
		return scheduler.schedule(task, delay, TimeUnit.MILLISECONDS);
	}

	/**
	 * @param task выполняемое задание.
	 * @param delay задержка до выполнения.
	 * @param interval интервал выполнения.
	 * @return ссылка на ожидающее задание.
	 */
	public ScheduledFuture<?> schedule(final Runnable task, final int delay, final int interval) {
		return scheduler.scheduleAtFixedRate(task, delay, interval, TimeUnit.MILLISECONDS);
	}

	/**
	 * @param callable выполняемое задание.
	 */
	public void submit(final Callable<?> callable) {
		executor.submit(callable);
	}

	/**
	 * @param task выполняемое задание.
	 */
	public void submit(final Runnable task) {
		executor.submit(task);
	}
}
